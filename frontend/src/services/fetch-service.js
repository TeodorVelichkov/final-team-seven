import { APP_URL } from "../common/Constants";

const FetchData = async (method, url, body = null, token = null) => {
  const res = await fetch(`${APP_URL}${url}`, {
    method,
    body: body ? JSON.stringify(body) : null,
    headers: {
      "Content-Type": "application/json",
      "Authorization": token ? `Bearer ${token}` : null
    }
  });
  const data = await res.json();
  if (!res.ok) {
    // Show error modal or something like that
    console.log(data);
  }

  return data;
};
export const signUp = FetchData.bind(null, "POST", "/users");
export const signIn = FetchData.bind(null, "POST", "/auth/login");
export const getAllFeed = FetchData.bind(null, "GET", "/feed");
export const createPost = FetchData.bind(null, "POST", "/posts");
export const updatePost = FetchData.bind(null, "PUT");
export const deletePost = FetchData.bind(null, "DELETE");
export const getPostWithComments = FetchData.bind(null, "GET");
export const getPost = FetchData.bind(null, "GET");
export const deleteComment = FetchData.bind(null, "DELETE");
export const postReact = FetchData.bind(null, "PUT");
export const addFriend = () => FetchData.bind(null, "POST");
export const getUser = () => FetchData.bind(null, "GET");
export const getTopComments = () => FetchData.bind(null, "GET", "/feed/popular");

