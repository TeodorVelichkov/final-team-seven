import { APP_URL } from "../common/Constants";
import {getAuthToken} from "./AuthService";

export const registerUser = (event) => {
    const formData = new FormData(event.target);

    return fetch(`${ APP_URL }/users`, {
        mode: "cors",
        method: "POST",
        body: formData,
    })
    .then(response => {
        return response.ok ? response.json()
            : response.text().then(text => { throw new Error(text); } );
    });
};

export const loginUser = (data) => {
    return fetch(`${ APP_URL }/auth/login`, {
        mode: "cors",
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }
    })
    .then(response => {
        return response.ok ? response.json()
            : response.text().then(text => { throw new Error(text); } );
    });
};

export const getUserByParams = (params) => {
    const URL = `${ APP_URL }/users?` + Object.keys(params).map(key => key + '=' + params[key]).join('&');

    return fetch(URL, {
        mode: "cors",
        method: "GET",
    })
        .then(response => {
            return response.ok ? response.json()
                : response.text().then(text => { throw new Error(text); } );
        });
}

export const getUserDetails = (id) => {
    return fetch(`${ APP_URL }/users/${ id }`, {
        mode: "cors",
        method: "GET",
        headers: {
            "Authorization": `Bearer ${getAuthToken()}`
        }
    })
        .then(response => {
            return response.ok ? response.json()
                : response.text().then(text => { throw new Error(text); } );
        });
}

export const getUserPosts = (id) => {
    return fetch(`${ APP_URL }/users/${ id }/posts`, {
        mode: "cors",
        method: "GET",
        headers: {
            "Authorization": `Bearer ${getAuthToken()}`
        }
    })
        .then(response => {
            return response.ok ? response.json()
                : response.text().then(text => { throw new Error(text); } );
        });
}

export const toggleUserFriend = (userId, friendId, method = "POST") => {
    return fetch(`${ APP_URL }/users/${userId}/friends/${friendId}`, {
        mode: "cors",
        method,
        headers: {
            "Authorization": `Bearer ${getAuthToken()}`
        }
    })
        .then(response => {
            return response.ok ? response.json()
                : response.text().then(text => { throw new Error(text); } );
        });
};

export const updateUserRequest = (event) => {
    const formData = new FormData(event.target);

    return fetch(`${APP_URL}/users/`, {
        mode: "cors",
        method: "PUT",
        body: formData,
        headers: {
            "Authorization": "Bearer " + getAuthToken()
        }
    })
        .then(response => {
            return response.ok ? response.json()
                : response.text().then(text => { throw new Error(text); } );
        });

}
