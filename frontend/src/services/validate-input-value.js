const validateInputValue = (validateValue, value, setIsValueValid) => {
  if (!validateValue(value)) {
    setIsValueValid(false);

    return false;
  }

  setIsValueValid(true);

  return true
};

export default validateInputValue;