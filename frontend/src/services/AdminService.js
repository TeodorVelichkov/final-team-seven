import {APP_URL} from "../common/Constants";
import {getAuthToken} from "./AuthService";

export const adminGetUsers = (params) => {
    const URL = `${ APP_URL }/users?` + Object.keys(params).map(key => key + '=' + params[key]).join('&');

    return fetch(`${ APP_URL }/users?${URL}`, {
        mode: "cors",
        method: "GET",
    })
        .then(response => {
            return response.ok ? response.json()
                : response.text().then(text => { throw new Error(text); } );
        });
};

export const adminUpdateUser = (event, user) => {
    const formData = new FormData(event.target);

    if(user.username === formData.get('username')) formData.delete('username');

    return fetch(`${APP_URL}/admin/users/${user.id}`, {
        mode: "cors",
        method: "PUT",
        body: formData,
        headers: {
            "Authorization": "Bearer " + getAuthToken()
        }
    })
        .then(response => {
            return response.ok ? response.json()
                : response.text().then(text => { throw text; } );
        });
};

export const adminBanUser = (id, data) => {

    return fetch(`${APP_URL}/users/${id}/ban`, {
        mode: "cors",
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-type": "application/json; charset=UTF-8",
            "Authorization": "Bearer " + getAuthToken()
        }
    })
        .then(response => {
            return response.ok ? response.json()
                : response.text().then(text => { throw text; } );
        });
};


export const adminGetUserPosts = (id) => {

    return fetch(`${APP_URL}/users/${id}/posts`, {
        mode: "cors",
        method: "GET",
        headers: {
            "Authorization": "Bearer " + getAuthToken()
        }
    })
        .then(response => {
            return response.ok ? response.json()
                : response.text().then(text => { throw text; } );
        });
};


export const adminUpdatePost = (event, id) => {
    const formData = new FormData(event.target);

    return fetch(`${APP_URL}/posts/${id}`, {
        mode: "cors",
        method: "PUT",
        body: formData,
        headers: {
            "Authorization": "Bearer " + getAuthToken()
        }
    })
        .then(response => {
            return response.ok ? response.json()
                : response.text().then(text => { throw text; } );
        });
};
