import {APP_URL} from "../common/Constants";

export const getMostPopularPost = () => {

    return fetch(`${APP_URL}/feed/popular`, {
        mode: "cors",
        method: "GET",
        headers: {
            "Content-type": "application/json; charset=UTF-8",
        }
    })
        .then(response => {
            return response.ok ? response.json()
                : response.text().then(text => { throw new Error(text); } );
        });

};
