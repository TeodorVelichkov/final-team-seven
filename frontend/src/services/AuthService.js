import jwt_decode from "jwt-decode";
import { AuthDefaults } from "../contexts/auth.context";

export const storeAuthToken = (token) => {
    localStorage.setItem("token", token);
};

export const removeAuthToken = () => localStorage.removeItem("token");

export const getAuthToken = () => localStorage.getItem("token");

export const checkIsUserAuthenticated = () => {
    const token = localStorage.getItem("token");
    if(token === null) return false;

    const decoded = jwt_decode(token);
    const currentTimeInSeconds = (new Date()).getTime() / 1000;

    return currentTimeInSeconds < decoded.exp;
};

export const getUserId = () => {
    const token = localStorage.getItem("token");
    if(token === null) return "";

    const decoded = jwt_decode(token);
    return decoded.id;
};

export const getUserRole = () => {
    const token = localStorage.getItem("token");
    if(token === null) return "";

    const decoded = jwt_decode(token);
    return decoded.role;
};

export const checkUserRole = (roles) => {
    if(!checkIsUserAuthenticated()) return false;

    return roles.some( role => role === getUserRole());
};

export const getUserName = () => {
    const token = localStorage.getItem("token");
    if(token === null) return "";

    const decoded = jwt_decode(token);
    return decoded.username;
};

export const getUserAvatar = () => {
    if(!checkIsUserAuthenticated() ) return '';

    return jwt_decode(getAuthToken()).avatar;
};

export const getUpdateAuthState = () => {
    const token = localStorage.getItem("token");
    if( token === null) return AuthDefaults;

    const decoded = jwt_decode(token);
    const auth = { ...AuthDefaults, user: { ...AuthDefaults.user } };

    console.log(decoded, auth);
    auth.user.username = decoded.username;
    auth.user.role = decoded.role;
    auth.user.id = decoded.id;
    auth.user.avatar = decoded.avatar;
    auth.isAuthenticated = true;

    console.log(auth);
    return auth;
};
