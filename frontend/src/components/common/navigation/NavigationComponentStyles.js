import styled from "styled-components";
import {Link} from "react-router-dom";
import {DEVICE} from "../../../common/Constants";

export const NavigationContainer = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  height: 100%;
  padding: 0 3.5rem;
`;

export const SearchNavigationContainer = styled.div`
  height: 100%;
  width: min(40rem, 45vw);
  padding: 1.2rem 0;
  text-decoration: none;
`;

export const UserPictureContainer = styled.div`
  position: relative;
  width: 4rem;
  height: 4rem;
  overflow: hidden;
  border-radius: 50%;
  cursor: pointer;
`;

export const UserPicture = styled.img`
  object-fit: cover;
  width: 100%;
  height: 100%;
`;

export const Logo = styled(Link)`
  text-decoration: none;
  font-family: 'Permanent Marker', sans-serif;
  font-size: 3.5rem;
  color: ${props => props.color || 'white'};

  @media (max-width: ${DEVICE.small}) {
    font-size: 2.1rem;
  }
`;
