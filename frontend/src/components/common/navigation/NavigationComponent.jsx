import React, {useContext, useState} from 'react';
import {
    Logo,
    NavigationContainer,
    SearchNavigationContainer,
    UserPicture,
    UserPictureContainer
} from "./NavigationComponentStyles";
import AuthContext from "../../../contexts/auth.context";
import { PLAIN_SERVER_URL } from "../../../common/Constants";
import UserMenuComponent from "./user-menu/UserMenuComponent";
import NavigationSearch from "./search/NavigationSearchComponent";

const Navigation = () => {

    const { userAuth } = useContext(AuthContext);
    const [ isMenuOpen, setIsMenuOpen ] = useState(false);

    return (
        <NavigationContainer>
            <Logo to={'/'}>
                AniManiacs
            </Logo>
            <SearchNavigationContainer>
                <NavigationSearch/>
            </SearchNavigationContainer>
            <UserPictureContainer onClick={ () => setIsMenuOpen(!isMenuOpen)}>
                <UserPicture src={`${PLAIN_SERVER_URL}${ userAuth.user.avatar }`} alt={'user picture'}/>
            </UserPictureContainer>
            { isMenuOpen && <UserMenuComponent setIsMenuOpen={setIsMenuOpen}/> }
        </NavigationContainer>
    );
};

export default Navigation;
