import React, { useState, useCallback } from 'react';
import { withRouter } from "react-router-dom";
import { debounce } from "lodash";
import {
    ResultsContainer,
    Search,
    SearchContainer,
    SearchedUserAvatar,
    SearchedUserContainer, SearchUsername
} from "./NavigationSearchStyle";
import {getUserByParams} from "../../../../services/UserService";
import {PLAIN_SERVER_URL} from "../../../../common/Constants";
import {UserPicture} from "../NavigationComponentStyles";

const NavigationSearch = () => {

    const [ results, setResults ] = useState([]);
    const [focused, setFocused] = React.useState(false)

    const onFocus = () => setFocused(true);
    const onBlur = () => setFocused(false);

    const search = ({ target: { value }}) => {
        if(value === '') return setResults([]);
        getUserByParams( { name: value, email: value})
            .then(response => setResults(response));
    };

    const memorized = useCallback(debounce(event => search(event), 500), []);

    return (
        <SearchContainer>
            <Search onFocus={onFocus} onChange={ (event) => memorized(event)}
                    type={'text'} placeholder={'Search by user name'}/>
            { (focused && results.length !== 0) && <ResultsContainer>
                { results.map(user =>
                    <SearchedUserContainer onBlur={onBlur} to={ `/user/${ user.id }`} key={ user.id }>
                        <SearchedUserAvatar>
                            <UserPicture src={`${PLAIN_SERVER_URL}${ user.avatar }`} alt={'user picture'} />
                        </SearchedUserAvatar>
                        <SearchUsername> { user.username } </SearchUsername>
                    </SearchedUserContainer> ) }
            </ResultsContainer> }
        </SearchContainer>
    );
};

export default withRouter(NavigationSearch);
