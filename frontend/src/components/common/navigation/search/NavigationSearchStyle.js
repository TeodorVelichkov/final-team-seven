import styled from "styled-components";
import {NavLink} from "react-router-dom";
import {COLORS} from "../../../../common/Constants";

export const SearchContainer = styled.div`
  position: relative;
  height: 100%;
  width: 100%;
`;

export const Search = styled.input`
  height: 100%;
  width: 100%;
  padding: 0 1rem;
  border: none;
  border-radius: 1.5rem;
  
  &:focus-visible {
    outline: none;
  }
`;

export const ResultsContainer = styled.div`
  position: absolute;
  top: 110%;
  left: 0;
  width: 100%;
  height: max-content;
  max-height: 20rem;
  padding: .5rem .3rem;
  border-radius: .5rem;
  background-color: white;
  overflow-y: scroll;
`;

export const SearchedUserContainer = styled(NavLink)`
  display: flex;
  text-decoration: none;
  padding: .4rem .3rem;
  border-radius: .3rem;
  
  &:hover {
    background-color: #a2c8ec;
  }
  
  & + & { margin-top: .5rem; }
`;

export const SearchUsername = styled.span`
  margin-left: 1.5rem;
  color: ${ COLORS.lightBlack };
  text-decoration: none;
`;

export const SearchedUserAvatar = styled.div`
  width: 3.2rem;
  height: 3.2rem;
  border-radius: 50%;
  overflow: hidden;
`;
