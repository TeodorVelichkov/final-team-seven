import styled from "styled-components";
import { DEVICE, LAYOUT } from "../../../../common/Constants";
import {NavLink} from "react-router-dom";
import {Icon} from "../../../../styles/genericStyledComponents/generic";

export const UserMenuWrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100vh;
  background-color: white;
  z-index: 100;

  @media (min-width: ${DEVICE.large}) {
    
    & {
      top: ${LAYOUT.navigationHeight};
      left:calc(100% - 15rem);
      height: 15rem;
      width: 15rem;
      background-color: white;
      border-bottom-left-radius: 1rem;
      border-bottom-right-radius: 1rem;
    }
  }
`;

export const ExitIcon = styled(Icon)`
  margin: auto 0 auto 2rem;
`;

export const UserMenuContainer = styled.div`
  display: grid;
  grid-template-rows: 7.5rem 1fr;
  width: 100%;
  height: 100%;

  @media (min-width: ${DEVICE.large}) {
    & {
      grid-template-rows: 1fr
    }
    
    ${ExitIcon} {
      display: none; 
    }
  }
`;

export const MenuLinkContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
`;

export const MenuLink = styled(NavLink)`
  position: relative;
  font-size: 2.5rem;
  text-decoration: none;
  color: ${props => props.color || '#444444'} ;
  
  &:hover::before, &:hover::after {
    transform: scale(1.3, 1);
  }
  
  &::before, &::after {
    content: '';
    position: absolute;
    bottom: 0;
    width: 50%;
    height: .3rem;
    transition: transform .4s ease-in-out;
    transform: scale(0, 1);
    background-color: #5c83a5;
  }
  
  &::before {
    transform-origin: right;
    border-bottom-left-radius: 1rem;
    border-top-left-radius: 1rem;
    left: 0;
  }
  
  &::after {
    left: 50%;
    transform-origin: left;
    border-bottom-right-radius: 1rem;
    border-top-right-radius: 1rem;
  }

  @media (min-width: ${DEVICE.large}) { 
    & {
      font-size: 2rem;
    }
    
  }
`;
