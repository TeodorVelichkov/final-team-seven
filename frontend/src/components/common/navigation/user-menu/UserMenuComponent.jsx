import React, {useContext} from 'react';
import {ExitIcon, MenuLink, MenuLinkContainer, UserMenuContainer, UserMenuWrapper} from "./UserMenuStyles";
import {faChevronCircleLeft} from "@fortawesome/free-solid-svg-icons";
import {getUpdateAuthState, removeAuthToken} from "../../../../services/AuthService";
import AuthContext from "../../../../contexts/auth.context";
import { withRouter } from "react-router-dom";
import NotificationContext from "../../../../contexts/notification.context";
import {COLORS, USER_ROLES} from "../../../../common/Constants";
import {Icon} from "../../../../styles/genericStyledComponents/generic";
import { faCogs, faSignOutAlt } from "@fortawesome/free-solid-svg-icons";

const UserMenuComponent = ({setIsMenuOpen, history}) => {

    const { userAuth, updateUserAuth } = useContext(AuthContext);
    const { setNotification } = useContext(NotificationContext);

    const logOutUser = (event) => {
        event.preventDefault();
        removeAuthToken();
        updateUserAuth(getUpdateAuthState());
        history.push('/authentication');
        setNotification({ active: true, message: "Logged out", color: COLORS.success });
    }

    return (
        <UserMenuWrapper>
            <UserMenuContainer>
                <ExitIcon onClick={ () => setIsMenuOpen(false) }
                    icon={faChevronCircleLeft} size={'3.5rem'} color={ 'green' }/>
                <MenuLinkContainer>
                    <MenuLink to={'/profile'} onClick={() => setIsMenuOpen(false)}> Profile </MenuLink>
                    <MenuLink to={'/settings'} onClick={() => setIsMenuOpen(false)}> Settings <Icon icon={faCogs}/> </MenuLink>
                    { userAuth.user.role === USER_ROLES.ADMIN &&
                        <MenuLink to={'/admin'}
                                  onClick={() => setIsMenuOpen(false)}> Admin panel </MenuLink>
                    }
                    <MenuLink to={'/authentication'} color={'red'} onClick={(event) => logOutUser(event)}> Log out <Icon icon={faSignOutAlt}/> </MenuLink>
                </MenuLinkContainer>
            </UserMenuContainer>
        </UserMenuWrapper>
    );
};

export default withRouter(UserMenuComponent);
