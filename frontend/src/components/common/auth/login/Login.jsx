import {useContext, useState} from "react";
import "../register/Register.css";
import useInputChange from "../../../../hooks/useInputChange";
import Modal from "../../../../common/modal/Modal";
import validateInputValue from "../../../../services/validate-input-value";
import Input from "../../../../common/input/Input";
import Button from "../../../../common/button/Button";
import "./Login.css";
import { loginUser } from "../../../../services/UserService";
import { getUpdateAuthState, storeAuthToken } from "../../../../services/AuthService";
import AuthContext from "../../../../contexts/auth.context";
import { withRouter } from "react-router-dom";
import NotificationContext from "../../../../contexts/notification.context";
import { COLORS } from "../../../../common/Constants";


const Login = (props) => {
  const [ username, usernameChangeHandler ] = useInputChange();
  const [ isUsernameValid, setIsUsernameValid ] = useState(false);
  const [ isUsernameTouched, setIsUsernameTouched ] = useState(false);

  const [ password, passwordChangeHandler ] = useInputChange();
  const [ isPasswordValid, setIsPasswordValid ] = useState(false);
  const [ isPasswordTouched, setIsPasswordTouched ] = useState(false);
  const { updateUserAuth  } = useContext(AuthContext);
  const { setNotification  } = useContext(NotificationContext);

  const submitHandler = evt => {
    evt.preventDefault();

    setIsUsernameTouched(true);
    setIsPasswordTouched(true);

    const usernameIsValid = validateInputValue((val) => {
      return val.length > 3;
    }, username, setIsUsernameValid);

    const passwordIsValid = validateInputValue((val) => {
      return val.length > 5;
    }, password, setIsPasswordValid);

    if (!usernameIsValid || !passwordIsValid) return;

    loginUser({ username, password })
        .then(response => {
            storeAuthToken( response.token );
            updateUserAuth( getUpdateAuthState() );
            setNotification({ active: true, message: "Successfully log in!", color: COLORS.success })
            props.history.push('/');
        })
        .catch(() => setNotification({ active: true, message: "Wrong credentials", color: COLORS.danger }));
  };

  return (
    <Modal>
      <section className="section-auth">
        <div className="section-login section-auth__right">
          <h4 className="section-register__heading">LOGIN</h4>
          <form onSubmit={submitHandler} className="register-form">
            <div className="form-field">
              <Input
                input={{
                  component: "input",
                  onChange: usernameChangeHandler,
                  value: username,
                  type: "text",
                  name: "username",
                  id: "username",
                  className: !isUsernameValid && isUsernameTouched ? "error-field" : undefined,
                  placeholder: "Username"
                }}
              />
              {!isUsernameValid && isUsernameTouched &&
              <p className="error-message">Username must be longer than 3 characters</p>}
            </div>
            <div className="form-field">
              <Input
                input={{
                  component: "input",
                  onChange: passwordChangeHandler,
                  value: password,
                  type: "password",
                  name: "password",
                  id: "password",
                  className: !isPasswordValid && isPasswordTouched ? "error-field" : undefined,
                  placeholder: "Password"
                }}
              />
              {!isPasswordValid && isPasswordTouched &&
              <p className="error-message">Incorrect password</p>}
            </div>
            <Button button={{ type: "submit", className: "cta cta--login" }}>Login</Button>
          </form>
        </div>

        {/*TODO if the user is not registered and click the register button functionality*/}
        {/*<button>register</button>*/}

      </section>
    </Modal>
  );
};

export default withRouter(Login);
