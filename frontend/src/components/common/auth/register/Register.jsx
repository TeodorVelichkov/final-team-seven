import { useContext, useState } from "react";
import { withRouter } from "react-router-dom";
import "./Register.css";
import useInputChange from "../../../../hooks/useInputChange";
import Modal from "../../../../common/modal/Modal";
import validateInputValue from "../../../../services/validate-input-value";
import Input from "../../../../common/input/Input";
import Button from "../../../../common/button/Button";
import { loginUser, registerUser } from "../../../../services/UserService";
import { getUpdateAuthState, storeAuthToken } from "../../../../services/AuthService";
import { COLORS } from "../../../../common/Constants";
import AuthContext from "../../../../contexts/auth.context";
import NotificationContext from "../../../../contexts/notification.context";


const Register = (props) => {
  const [ username, usernameChangeHandler ] = useInputChange();
  const [ isUsernameValid, setIsUsernameValid ] = useState(false);
  const [ isUsernameTouched, setIsUsernameTouched ] = useState(false);

  const [ email, emailChangeHandler ] = useInputChange();
  const [ isEmailValid, setIsEmailValid ] = useState(false);
  const [ isEmailTouched, setIsEmailTouched ] = useState(false);

  const [ password, passwordChangeHandler ] = useInputChange();
  const [ isPasswordValid, setIsPasswordValid ] = useState(false);
  const [ isPasswordTouched, setIsPasswordTouched ] = useState(false);
  const { updateUserAuth } = useContext(AuthContext);
  const { setNotification } = useContext(NotificationContext);

  const submitHandler = event => {
    event.preventDefault();

    setIsUsernameTouched(true);
    setIsEmailTouched(true);
    setIsPasswordTouched(true);

    const usernameIsValid = validateInputValue((val) => {
      return val.length > 3;
    }, username, setIsUsernameValid);

    const emailIsValid = validateInputValue((val) => {
      return val;
    }, email, setIsEmailValid);

    const passwordIsValid = validateInputValue((val) => {
      return val.length > 5;
    }, password, setIsPasswordValid);

    if (!usernameIsValid || !emailIsValid || !passwordIsValid) return;

    registerUser(event)
      .then(() => {
        loginUser({ username, password })
          .then(response => {
            storeAuthToken(response.token);
            updateUserAuth(getUpdateAuthState());
            setNotification({ active: true, message: "Successfully registered!", color: COLORS.success });
            props.history.push("/");
          });
      })
      .catch(error => console.log(error));
  };

  return (
    <Modal>
      <section className="section-register">
        <h4 className="section-register__heading">REGISTER</h4>
        <form onSubmit={submitHandler} className="register-form">
          <div className="form-field">
            <Input
              input={{
                component: "input",
                onChange: usernameChangeHandler,
                value: username,
                type: "text",
                name: "username",
                id: "username",
                className: !isUsernameValid && isUsernameTouched ? "error-field" : undefined,
                placeholder: "Username",
                autoFocus: true
              }}
            />
            {!isUsernameValid && isUsernameTouched &&
            <p className="error-message">Username must be longer than 3 characters</p>}
          </div>
          <div className="form-field">
            <Input
              input={{
                component: "input",
                onChange: emailChangeHandler,
                value: email,
                type: email,
                name: "email",
                id: "email",
                className: !isEmailValid && isEmailTouched ? "error-field" : undefined,
                placeholder: "Email"
              }}
            />
            {!isEmailValid && isEmailTouched &&
            <p className="error-message">Incorrect email</p>}
          </div>
          <div className="form-field">
            <Input
              input={{
                component: "input",
                onChange: passwordChangeHandler,
                value: password,
                type: "password",
                name: "password",
                id: "password",
                className: !isPasswordValid && isPasswordTouched ? "error-field" : undefined,
                placeholder: "Password"
              }}
            />
            {!isPasswordValid && isPasswordTouched &&
            <p className="error-message">Incorrect password</p>}
          </div>
          <Input
            input={{
              component: "input",
              type: "file",
              name: "file",
            }}
          />
          <Button button={{ type: "submit", className: "cta cta--register" }}>Register</Button>
        </form>
      </section>
    </Modal>
  );
};

export default withRouter(Register);
