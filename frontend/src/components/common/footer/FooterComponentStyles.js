import styled from "styled-components";

export const FooterContainer = styled.footer`
  position: relative;
  width: 100%;
  height: 100%;
`;

export const WavesContainer = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
`;

export const Wave = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: ${ props => props.color || 'red'};
  animation: wave 10s ${ props => props.delay || '0s' } ease-in-out infinite;
  
  @keyframes wave {
    0%, 100% {
      clip-path: polygon(0 39%, 13% 32%, 30% 28%, 43% 34%, 54% 43%, 70% 48%, 84% 54%, 100% 50%, 100% 100%, 0 100%);
    }
    50% {
      clip-path: polygon(0 62%, 20% 57%, 36% 51%, 49% 42%, 61% 34%, 75% 31%, 89% 35%, 100% 44%, 100% 100%, 0 100%);
    }
  }
`;
