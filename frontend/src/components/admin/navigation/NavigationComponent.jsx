import React from "react";
import { useRouteMatch } from "react-router-dom";
import { NavigationLink, NavigationWrapper } from "./NavigationStyles";

const NavigationComponent = () => {
  const { url } = useRouteMatch();

    console.log(url)
  return (
    <NavigationWrapper>
      <NavigationLink to={`${url}/users`}> Users </NavigationLink>
      <NavigationLink exact to={`${url}`}> Home </NavigationLink>
      <NavigationLink exact to={"/"}> Public </NavigationLink>
    </NavigationWrapper>
  );
};

export default NavigationComponent;
