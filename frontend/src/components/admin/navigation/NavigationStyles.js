import { NavLink } from "react-router-dom";
import styled from "styled-components";
import { COLORS } from "../../../common/Constants";

export const NavigationWrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  justify-content: center;
  background-color: ${ COLORS.navy };
`;

export const NavigationLink = styled(NavLink)`
  color: ${ COLORS.white };
  padding: .8rem 0 .8rem 1.2rem;
  font-size: 1.9rem;
  text-decoration: none;
  font-weight: 700;
  text-transform: uppercase;
  
  &.active {
    color: ${ COLORS.navy };
    background-color: ${ COLORS.white };
  }
  
  &:hover {
    color: ${ COLORS.navy };
    background-color: ${ COLORS.white };
  }
  
`;
