import React, {useEffect, useState} from "react";
import { withRouter } from "react-router-dom";
import {
    TableBodyRow,
    TableHead,
    TableStyledComponent,
    GenericManagementWrapper,
    PaginationContainer, TableSearchInput, HeaderTableContainer
} from "./UserManagementStyles";
import {adminGetUsers} from "../../../services/AdminService";
import UserManageRow from "./UserManageRow";
import {getUserDetails} from "../../../services/UserService";
import ReactPaginate from "react-paginate";
import {COLORS} from "../../../common/Constants";

const UserManagementComponent = ({match}) => {
  const [ user, setUser ] = useState(null);
  const [ users, setUsers ] = useState([]);
  const [ displayed, setDisplayed ] = useState([]);
  const [ pageCount, setPageCount ] = useState(5);
  const [ lastCount, setLastCount ] = useState(0);
  const [ count ] = useState(3);
  const [ update, setUpdate ] = useState(false);

  const { id } = match.params;

  const getAllUsers = () => {
     adminGetUsers({})
         .then(response => {
             setUsers(response);
             setDisplayed(response.slice(0, Math.min(response.length, count)) )
             setPageCount(Math.ceil(response.length / count));
         });
  };

  const getAllUserFriends = () => {
      getUserDetails(id)
          .then(response => {
              setUser(response);
              setUsers([ ...response.friends ]);
              setDisplayed(response.friends.slice(0, Math.min(response.friends.length, count)) );
              setPageCount(Math.ceil(response.friends.length / count));
          });
  };

  useEffect(() => {
      id === undefined ? getAllUsers() : getAllUserFriends();

      return () => setUser(null);
  }, [update, id]);

    const handleChangePage = (event) => {
        setLastCount(event.selected);
        setDisplayed(users.slice((count * event.selected), Math.min(users.length, (count * event.selected) + count)) )
    };

    const handleSearch = (event) => {
        if(event.target.value === '') {
            return setDisplayed(users.slice((count * lastCount), Math.min(users.length, (count * lastCount) + count)) );
        }

        const filtered = users.filter(user => user.username.includes(event.target.value) || (user.id + '').includes(event.target.value) );
        setDisplayed(filtered);
    }

  return (
    <>
        <GenericManagementWrapper>
             <HeaderTableContainer>
                <h2> { user !== null ? <span> <span style={{'color': COLORS.danger}}> { `${user.username}'s` } </span> friends: </span> : 'Users:'}</h2>
                <label>
                    Search:
                    <TableSearchInput onChange={(event) => handleSearch(event)} placeholder={'ID or username'}/>
                </label>
            </HeaderTableContainer>
          <TableStyledComponent>
              <TableBodyRow>
                  <TableHead> ID </TableHead>
                  <TableHead> username </TableHead>
                  <TableHead> avatar </TableHead>
                  <TableHead> role </TableHead>
                  <TableHead> friends </TableHead>
                  <TableHead> posts </TableHead>
                  <TableHead> status </TableHead>
                  <TableHead> action </TableHead>
              </TableBodyRow>
            { displayed.length !== 0 && displayed.map( user => <UserManageRow key={user.id} update={update} setUpdate={setUpdate} user={user}/>) }
          </TableStyledComponent>
            <PaginationContainer>
                <ReactPaginate
                    nextLabel="next >"
                    onPageChange={handleChangePage}
                    pageRangeDisplayed={count}
                    pageCount={pageCount}
                    previousLabel="< previous"
                    pageClassName="page-item"
                    pageLinkClassName="page-link"
                    previousClassName="page-item"
                    previousLinkClassName="page-link"
                    nextClassName="page-item"
                    nextLinkClassName="page-link"
                    breakLabel="..."
                    breakClassName="page-item"
                    breakLinkClassName="page-link"
                    containerClassName="pagination"
                    activeClassName="active"
                    renderOnZeroPageCount={null}
                />
            </PaginationContainer>

        </GenericManagementWrapper>
    </>
  );
};

export default withRouter(UserManagementComponent);
