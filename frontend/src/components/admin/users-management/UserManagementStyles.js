import styled, { css } from "styled-components";
import { COLORS } from "../../../common/Constants";

const shared = css`
  border: 1px solid black;
`;

export const GenericManagementWrapper = styled.div`
  width: 100%;
  padding: 0 1.2rem;
`;

export const HeaderTableContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 1.8rem;
`;

export const TableSearchInput = styled.input`
  width: max-content;
  max-width: ${props => props.size};
  padding: .1rem .2rem;
  margin: auto auto auto 1.2rem;
  border: 2px solid ${ COLORS.navy };
  border-radius: 5px;
  font-size: 1.8rem;

  &:focus-visible {
    outline: 2px solid ${ COLORS.navy };
  }
`;


export const TableStyledComponent = styled.div`
  display: table;
  border-collapse: collapse;
  width: 100%;
  ${ shared };
`;

export const TableHead = styled.th`
  text-align: center;
  ${ shared };
`;

export const TableBodyRow = styled.form`
  display: table-row !important;
  height: max-content;
  
  &.active {
    border: 3px solid ${ COLORS.danger };
  }
`;

export const PaginationContainer = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 1.5rem;
`;

export const TableData = styled.div`
  height: 100%;
  display: table-cell;
  text-align: center;
  padding: .6rem .3rem;
  ${ shared };
`;

export const CenteredContainer = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  justify-content: space-around;
  align-items: center;
`;

export const UserRowPictureContainer = styled.div`
  width: 3.7rem;
  height: 3.7rem;
  border-radius: 50%;
  overflow: hidden;
`;

export const UserRowPicture = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const GenericButton = styled.button`
  display: flex;
  //margin: .8rem 0 .8rem 0;
  width: max-content;
  padding: .6rem 1.2rem;
  color: ${ props => props.color };
  font-size: 1.6rem;
  font-weight: 600;
  text-transform: uppercase;
  background-color: ${ COLORS.white };
  border: 1px solid ${ props => props.color };
  border-radius: 5px;
  
  &:hover {
    color: ${ COLORS.white };
    background-color: ${ props => props.color };
    cursor: pointer;
  }
`;

export const GenericTextInput = styled.input`
  display: flex;
  width: max-content;
  max-width: ${props => props.size};
  padding: .1rem .2rem;
  margin: auto;
  border: none;
  border-radius: 5px;
  font-size: 1.8rem;
  
  &:focus-visible {
    outline: 2px solid ${ COLORS.navy }; 
  }
`;

export const SelectTable = styled.select`
  display: flex;
  margin: auto;
  background-color: transparent;
  border: none;
  cursor: pointer;
  font-size: 1.8rem;

  &:focus-visible {
    outline: none;
  }
`;

export const BannedHeader = styled.h1`
  width: 100%;
  color: ${ COLORS.danger };
  text-transform: uppercase;
  text-align: center;
`;

export const ModalWrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100vh;
  display: grid;
  place-items: center;
 
  z-index: 100;
`;

export const ModalContainer = styled.form`
    position: relative;
    width: ${ props => props.width || '45rem' };
    height: ${ props => props.height || '57rem' };
    display: flex;
    flex-direction: column;
    justify-content: center;
    gap: .6rem;
    align-items: flex-start;
    padding: 0 2.5rem;

    background-color: rgba(207, 207, 207, .8);
    border-radius: 5px;    
`;

export const CloseContainer = styled.div`
    position: absolute;
    top: 2%;
    right: 3%;
    color: red;
`;
