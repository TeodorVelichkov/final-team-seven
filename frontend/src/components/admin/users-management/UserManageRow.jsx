import {faTimes, faUpload} from "@fortawesome/free-solid-svg-icons";
import React, {useContext, useState} from "react";
import {APP_URL, COLORS, USER_ROLES} from "../../../common/Constants";
import { withRouter } from "react-router-dom";
import useForm from "../../../hooks/useForm";

import {
  CenteredContainer,
  TableBodyRow,
  TableData,
  GenericButton,
  GenericTextInput,
  UserRowPicture,
  UserRowPictureContainer,
  SelectTable, ModalWrapper, ModalContainer, BannedHeader, CloseContainer,
} from "./UserManagementStyles";
import {LabelPictureInput, PictureInput} from "../../../views/public/UserView/UserViewStyles";
import {Icon} from "../../../styles/genericStyledComponents/generic";
import {adminBanUser} from "../../../services/AdminService";
import NotificationContext from "../../../contexts/notification.context";
import {format} from "date-fns";

const UserManageRow = ({ user, update, setUpdate, history }) => {

  const [ isModalOpen, setIsModalOpen ] = useState(false);
  const [ banDetails, setBanDetails ] = useState({ reason: user.banReason, period: user.banDate });
  const { setNotification  } = useContext(NotificationContext);

  const banUser = (event) => {
    event.preventDefault();

    if(banDetails.reason === null || banDetails.period === null) return;
    if(user.banDate === banDetails.reason && user.banReason === banDetails.period) return;

    const date = new Date(banDetails.period);
    adminBanUser(user.id, {reason: banDetails.reason, period: date.getTime() / 1000 })
        .then(() => {
            setUpdate(!update);
            setNotification({ active: true, message: "User banned!", color: COLORS.success });
        }).catch(() =>
            setNotification({ active: true, message: "Something went wrong banning user!", color: COLORS.danger }) );
  };

  const updateUser = () => {
    setNotification({ active: true, message: "User successfully updated!", color: COLORS.success })
  };

  const formatDate = (date) => {
    return format(new Date(date), 'yyyy-MM-dd');
  }

  const [, handleChange, handleSubmit ] = useForm(updateUser);
  const userRoles = [{ key: 'user', value: USER_ROLES.USER }, { key: 'admin', value: USER_ROLES.ADMIN } ];

  return (
    <>
      {/*className={ (user.banDate !== null && user.banReason !== null)  ? "active" : ""}*/}
      <TableBodyRow onSubmit={ handleSubmit } >
        <TableData> { user.id } </TableData>
        <TableData>
          <GenericTextInput onChange={ handleChange } size={'20ch'} name="username" type={"text"} defaultValue={ user.username }/>
        </TableData>
        <TableData>
          <CenteredContainer>
            <UserRowPictureContainer>
              <UserRowPicture src={`${APP_URL}/${user.avatar}`} alt="user avatar picture"/>
            </UserRowPictureContainer>
            <LabelPictureInput>
              <Icon icon={ faUpload }/> picture
              <PictureInput onChange={ handleChange } name="avatar" type="file"/>
            </LabelPictureInput>
          </CenteredContainer>
        </TableData>
        <TableData>
          <SelectTable name="role" onChange={ handleChange } defaultValue={ user.role }>
            { userRoles.map( role => <option key={role.value} value={role.value}> { role.key }</option>)}
          </SelectTable>
        </TableData>
        <TableData>
          <GenericButton onClick={() => history.push(`/admin/users/${user.id}`)}
              color={ COLORS.lightBlue} type={"button"}> Friends </GenericButton>
        </TableData>
        <TableData>
          <GenericButton onClick={() => history.push(`/admin/users/${user.id}/posts`)}
              color={ COLORS.lightBlue} type={"button"}> Posts </GenericButton>
        </TableData>
        <TableData>
          <GenericButton color={ (user.banDate === null && user.banReason === null) ? COLORS.lightBlue : COLORS.danger }
                         onClick={ () => setIsModalOpen(true) } type={"button"}> See Details </GenericButton>
        </TableData>
        <TableData>
          <GenericButton color={ COLORS.success } type={"submit"}> update </GenericButton> </TableData>
      </TableBodyRow>
      { isModalOpen &&
          <ModalWrapper style={{ "backdropFilter": "blur(10px)", "backgroundColor": "rgba(250,250,250,.3)" }}>
            <ModalContainer onSubmit={(event) => banUser(event)}>
              { (user.banDate !== null && user.banReason !== null) && <BannedHeader> Banned! </BannedHeader> }
              <label>
                Reason: <GenericTextInput size={'100%'}
                                          onChange={ (event) => setBanDetails({ ...banDetails, reason: event.target.value }) }
                                          value={ banDetails.reason } type={"text"}/>
              </label>
              <label>
                Expire: <input type="date"
                               onChange={ (event) => setBanDetails({ ...banDetails, period: event.target.value }) }
                               value={ formatDate(banDetails.period) }/>
              </label>
              <GenericButton color={ COLORS.success } type={"submit"}> Update </GenericButton>
              <CloseContainer>
                <span onClick={ () => setIsModalOpen(false) }> <Icon icon={faTimes} color={COLORS.black}/> </span>
              </CloseContainer>
            </ModalContainer>
          </ModalWrapper>
      }
    </>
  );
};

export default withRouter(UserManageRow);
