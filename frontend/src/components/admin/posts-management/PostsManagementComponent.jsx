import React, {useEffect, useState} from 'react';
import {
    GenericManagementWrapper, HeaderTableContainer, PaginationContainer,
    TableBodyRow,
    TableHead, TableSearchInput,
    TableStyledComponent
} from "../users-management/UserManagementStyles";
import {adminGetUserPosts} from "../../../services/AdminService";
import PostManageRow from "./PostManageRow";
import ReactPaginate from "react-paginate";


const PostsManage = ({ match }) => {

    const [ update, setUpdate ] = useState(false);
    const [ posts , setPosts ] = useState([]);
    const [ displayed, setDisplayed ] = useState([]);
    const [ pageCount, setPageCount] = useState(5);
    const [ lastCount, setLastCount] = useState(0);
    const [ count ] = useState(3);
    const { id } = match.params;

    useEffect(() => {

        adminGetUserPosts(id)
            .then(response => {
                setPosts(response);
                setDisplayed(response.slice(0, Math.min(response.length, count)) );
                setPageCount(Math.ceil(response.length / count));
            })
            .catch(error => console.log(error));

    }, [id, update ]);

    const handleSearch = (event) => {
        if(event.target.value === '') {
            return setDisplayed(posts.slice((count * lastCount), Math.min(posts.length, (count * lastCount) + count)) );
        }

        const filtered = posts.filter(post => post.content.includes(event.target.value) || (post.id + '').includes(event.target.value) );
        setDisplayed(filtered);
    }

    const handlePageClick = (event) => {
        setLastCount(event.selected);
        setDisplayed(posts.slice((count * event.selected), Math.min(posts.length, (count * event.selected) + count)) )
    };

    return (
        <GenericManagementWrapper>
            <HeaderTableContainer>
                <h1> Posts: </h1>
                <label>
                    Search:
                    <TableSearchInput onChange={(event) => handleSearch(event)} placeholder={'ID or content'}/>
                </label>
            </HeaderTableContainer>
            <TableStyledComponent>
                <TableBodyRow>
                    <TableHead> ID </TableHead>
                    <TableHead> content </TableHead>
                    <TableHead> latitude </TableHead>
                    <TableHead> longitude </TableHead>
                    <TableHead> state </TableHead>
                    <TableHead> comments </TableHead>
                    <TableHead> visibility </TableHead>
                    <TableHead> action </TableHead>
                </TableBodyRow>
                { displayed.length !== 0 && displayed.map( post => <PostManageRow key={post.id} post={post} update={update} setUpdate={setUpdate}/>) }
            </TableStyledComponent>
            <PaginationContainer>
                <ReactPaginate
                    nextLabel="next >"
                    onPageChange={handlePageClick}
                    pageRangeDisplayed={count}
                    pageCount={pageCount}
                    previousLabel="< previous"
                    pageClassName="page-item"
                    pageLinkClassName="page-link"
                    previousClassName="page-item"
                    previousLinkClassName="page-link"
                    nextClassName="page-item"
                    nextLinkClassName="page-link"
                    breakLabel="..."
                    breakClassName="page-item"
                    breakLinkClassName="page-link"
                    containerClassName="pagination"
                    activeClassName="active"
                    renderOnZeroPageCount={null}
                />
            </PaginationContainer>

        </GenericManagementWrapper>
    );
};

export default PostsManage;
