import React, {useContext, useState} from 'react';
import {
    CloseContainer, GenericButton,
    GenericTextInput, ModalContainer, ModalWrapper, SelectTable, TableBodyRow,
    TableData,
} from "../users-management/UserManagementStyles";
import {COLORS, POST_STATES, POST_VISIBILITIES} from "../../../common/Constants";
import {Icon} from "../../../styles/genericStyledComponents/generic";
import {faTimes} from "@fortawesome/free-solid-svg-icons";
import useForm from "../../../hooks/useForm";
import SinglePostView from "../../../views/public/SinglePostView/SinglePostView";
import {ModalSinglePostWrapper} from "./PostManagementStyles";
import {adminUpdatePost} from "../../../services/AdminService";
import NotificationContext from "../../../contexts/notification.context";

const PostManageRow = ({ post, update, setUpdate }) => {
    const [ isModalOpen, setIsModalOpen ] = useState(false);
    const { setNotification  } = useContext(NotificationContext);
    const visibilities = [{ key: 'public', value: POST_VISIBILITIES.PUBLIC }, { key: 'private', value: POST_VISIBILITIES.PRIVATE } ];
    const states = [{ key: 'deleted', value: POST_STATES.DELETED }, { key: 'available', value: POST_STATES.AVAILABLE } ];

    const updatePost = (event) => {
        adminUpdatePost(event, post.id)
            .then(() => {
                setUpdate(!update);
                setNotification({ active: true, message: "User banned!", color: COLORS.success });
            })
            .catch(() =>
                setNotification({ active: true, message: "Something went wrong updating post!", color: COLORS.danger }) );
    }

    const [,handleChange, handleSubmit ] = useForm(updatePost);
    return (
        <>
            <TableBodyRow onSubmit={ event => handleSubmit(event) } >
                <TableData> { post.id } </TableData>
                <TableData>
                    <GenericTextInput onChange={ handleChange } size={'25ch'} name="content" type={"text"} defaultValue={ post.content }/>
                </TableData>
                <TableData>
                    <GenericTextInput onChange={ handleChange } size={'10ch'} name="latitude" type={"text"} defaultValue={ post.latitude }/>
                </TableData>
                <TableData>
                    <GenericTextInput onChange={ handleChange } size={'10ch'} name="longitude" type={"text"} defaultValue={ post.longitude }/>
                </TableData>
                <TableData>
                    <SelectTable name="isDeleted" onChange={ handleChange } defaultValue={ post.isDeleted }>
                        { states.map( state => <option key={state.value} value={state.value}> { state.key }</option>)}
                    </SelectTable>
                </TableData>

                <TableData>
                    <GenericButton color={ COLORS.success }
                                   onClick={ () => setIsModalOpen(true) } type={"button"}> Check comments </GenericButton>
                </TableData>
                <TableData>
                    <SelectTable name="isPublic" onChange={ handleChange } defaultValue={ post.isPublic }>
                        { visibilities.map( role => <option key={role.value} value={role.value}> { role.key }</option>)}
                    </SelectTable>
                </TableData>
                <TableData>
                    <GenericButton color={ COLORS.success } type={"submit"}> update </GenericButton> </TableData>
            </TableBodyRow>
            { isModalOpen &&
                <ModalWrapper style={{ "backdropFilter": "blur(10px)", "backgroundColor": "rgba(250,250,250,.3)" }}>
                    <ModalContainer width={'140rem'} height={'100vh'}>
                        <ModalSinglePostWrapper>
                            <SinglePostView id={post.id}/>
                        </ModalSinglePostWrapper>
                        <CloseContainer>
                            <span onClick={ () => setIsModalOpen(false) }> <Icon icon={faTimes} color={COLORS.black}/> </span>
                        </CloseContainer>
                    </ModalContainer>
                </ModalWrapper>
            }
        </>
    );
};

export default PostManageRow;
