import "./test.css"

const Test = ({baseClass, otherClasses}) => {
  return (
    <div className={`${baseClass} ${otherClasses}`}/>
  )
}

export default Test;