import React from 'react';
import {
    AuthNavigationButton,
    LeftSection,
    NavigationAboutButton,
    NavigationContainer,
    RightSection
} from "./NavigationAuthStyles";
import { COLORS } from "../../../common/Constants";
import {Logo} from "../../common/navigation/NavigationComponentStyles";

const NavigationAuthComponent = ({ openAuthRegister, openAuthLogin }) => {
    return (
        <NavigationContainer>
            <LeftSection>
                <Logo color={COLORS.lightRed}> AniManiacs </Logo>
            </LeftSection>
            <RightSection>
                <NavigationAboutButton> About us </NavigationAboutButton>
                <AuthNavigationButton onClick={ openAuthRegister } background={ COLORS.lightRed } color={ COLORS.white }> Sign up </AuthNavigationButton>
                <AuthNavigationButton onClick={ openAuthLogin } color={ COLORS.white } background={ COLORS.success }> Sign in </AuthNavigationButton>
            </RightSection>
        </NavigationContainer>
    );
};

export default NavigationAuthComponent;
