import styled from "styled-components";

export const NavigationContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  height: 100%;
  padding: 0 3.5rem;
`;

export const LeftSection = styled.div``;

export const RightSection = styled.div`
  display: flex;
  align-items: center;
`;

export const NavigationAboutButton = styled.button`
  background: transparent;
  border: none;
  font-weight: 600;
  font-size: 1.8rem;
  cursor: pointer;
`;


export const AuthNavigationButton = styled.button`
  border: none;
  padding: .3rem 1.2rem;
  font-size: 1.8rem;
  border-radius: 1rem;
  font-weight: 600;
  color: ${ props => props.color };  
  background-color: ${ props => props.background };
  transition: transform .3s ease-in-out;
  cursor: pointer;
  
  &:hover {
    transform: scale(1.05);
    color: ${ props => props.background };
    background-color: ${ props => props.color };
  }
  
  & + & {
    margin-left: 1.8rem;
  }

  ${ NavigationAboutButton } + & {
    margin-left: 1.8rem;
  }
  
`;
