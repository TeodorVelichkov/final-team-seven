import { useState, useEffect } from "react";
import "./TopPostsSection.css";
import { APP_URL } from "../../../common/Constants";
import TopPostCard from "../top-post-card/TopPostCard";

/*
content: "post 1 dsa"
createdOn: "Fri Dec 03 2021 15:19:51 GMT+0200 (Eastern European Standard Time)"
embed: null
id: 10
likesCount: 3
picture: "grass-156c71e2fe5317f6.jpg"
*/
const TopPostsSection = () => {
  // eslint-disable-next-line no-unused-vars
  const [ topPosts, setTopPosts ] = useState([]);

  useEffect(() => {
    fetch(`${APP_URL}/feed/popular`)
      .then(res => res.json())
      .then(data => {
          console.log(data.length, 'heeere');
        setTopPosts(() => data);
      })
      .catch(e => console.log(e));
  }, []);

  return (
    <section className="section-top-posts">
      <h3 className="section-friends__heading">Top 20</h3>
      <ul className="top-list">
        {
          topPosts.map(curPost => {
            return <TopPostCard key={curPost.id} post={curPost}/>
          })
        }
      </ul>
    </section>
  );
};

export default TopPostsSection;
