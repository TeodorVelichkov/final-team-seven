import Modal from "../../../common/modal/Modal";
import Button from "../../../common/button/Button";

const DeleteModal = ({onClose, onDeletePost}) => {
  return (
    <Modal onClose={onClose}>
      <div>Are you sure?</div>
      <div className="delete-modal-actions">
        <Button button={{onClick: onClose}}>Cancel</Button>
        <Button button={{onClick: onDeletePost}}>Confirm</Button>
      </div>
    </Modal>
  )
}

export default DeleteModal;
