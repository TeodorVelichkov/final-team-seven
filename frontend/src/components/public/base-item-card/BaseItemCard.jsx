import { useState } from "react";
import "./BaseItemCard.css";
import { BsThreeDots } from "react-icons/bs";
import { MdOutlinePublic } from "react-icons/md";
import { FaUserFriends } from "react-icons/fa";
import MenuTriangle from "../menu-triangle/MenuTriangle";
import { Link } from "react-router-dom";
import YouTube from "../youtube/YouTube";

// eslint-disable-next-line no-unused-vars
const BaseItemCard = ({ item, onEdit, onDelete, editable = true, isInSingleView = false }) => {
  // eslint-disable-next-line no-unused-vars
  const { author, content, createdOn, updatedOn, embed, id, picture, isPublic } = item;

  const [ isPostSettingsShown, setIsPostSettingsShown ] = useState(false);

  const postMenuIconClickHandler = () => setIsPostSettingsShown(!isPostSettingsShown);

  const editClickHandler = () => onEdit(item);

  const deleteClickHandler = () => onDelete(id);

  return (
    <>
      <div className="post__header">
        <div className="post__main-info">
          <Link to={`/user/${author.id}`} className="post__avatar">
            <img src={`http://localhost:5000/${author.avatar ? author.avatar : "default.png"}`} alt="avatar" className="post__avatar--img"/>
          </Link>
          <span className="post__username">{author.username}</span>
        </div>
        {editable && <div className="post__main-actions">
          <div className="post__settings-menu-icon" onClick={postMenuIconClickHandler}>
            <BsThreeDots>post menu icon</BsThreeDots>
          </div>
          {
            isPostSettingsShown && <ul className="post__settings">
              <MenuTriangle/>
              <li
                onClick={editClickHandler}
                className="post__settings-item post__settings-item--edit"
              >
                Edit
              </li>
              <li
                onClick={deleteClickHandler}
                className="post__settings-item post__settings-item--delete"
              >
                Delete
              </li>
            </ul>
          }
        </div>}
      </div>
      <div className="post__body">
        <div className="post__content">
          <div className="post__text">
            {content}
          </div>
          {!isInSingleView &&
          <Link to={`/posts/${item.id}`} className="post-img-box">
            {picture && <img className="post-img" src={`http://localhost:5000/${picture}`} alt=""/>}
          </Link>}
          {embed && <YouTube link={embed}/>}
        </div>
        <div className="post__sub-content">
          <div>
            {item.comments && isPublic === 1 && <MdOutlinePublic className="public-or-private-icon"/>}
            {item.comments && isPublic !== 1 && <FaUserFriends className="public-or-private-icon"/>}
            Created: {new Date(createdOn).toLocaleDateString()}</div>
          {/*<div>Time: {createdOn.toLocaleTimeString()}</div>*/}
        </div>
      </div>
    </>
  );
};

export default BaseItemCard;
