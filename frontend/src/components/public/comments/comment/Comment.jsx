import { useState, useContext } from "react";
import "./Comment.css";
import BaseItemCard from "../../base-item-card/BaseItemCard";
import CommentForm from "../../forms/comment-form/CommentForm";
// eslint-disable-next-line no-unused-vars

import AuthContext from "../../../../contexts/auth.context";
import { USER_ROLES } from "../../../../common/Constants";
import ReactionsActions from "../../reactions/ReactionsActions";
import useReactions from "../../../../hooks/useReactions";
import { getPost, postReact } from "../../../../services/fetch-service";
import { getAuthToken } from "../../../../services/AuthService";
import ReactionsMade from "../../reactions/ReactionsMade";


const Comment = ({ onAdd, onEdit, onDelete, comment, post }) => {

  const [ commentToBeEdited, setCommentToBeEdited ] = useState(null);
  const goInEditMode = () => setCommentToBeEdited(() => comment);
  const stopEditMode = () => setCommentToBeEdited(() => null);
  const deleteCommentHandler = () => onDelete(comment);
  // eslint-disable-next-line no-unused-vars
  const { reactions, reactionsMade, react, countReactions } = useReactions();
  const { userAuth } = useContext(AuthContext);

  const reactionClickHandler = async reaction => {
    await postReact(`/posts/${post.id}/react`, { reaction: reaction + 1 }, getAuthToken());

    const { likes } = await getPost(`/posts/${post.id}`, undefined, getAuthToken());

    react(likes || []);
  };

  return (
    <>
      {commentToBeEdited && <CommentForm
        post={post}
        commentToBeEdited={commentToBeEdited}
        onAdd={onAdd}
        onEdit={onEdit}
        onCancel={stopEditMode}
      />}
      {!commentToBeEdited &&
      <section className="comment-box">
        <BaseItemCard
          item={comment}
          onDelete={deleteCommentHandler}
          onEdit={goInEditMode}
          editable={comment.author.id === userAuth.user.id || userAuth.user.role === USER_ROLES.ADMIN}
        />
        <ReactionsMade
          reactions={reactions}
          reactionsMade={reactionsMade}
          countReactions={countReactions}
        />
        <ReactionsActions
          reactions={reactions}
          onReact={reactionClickHandler}
        />
      </section>
      }
    </>
  );
};

export default Comment;
