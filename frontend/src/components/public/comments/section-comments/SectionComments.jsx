import { useState } from "react";
import "./SectionComments.css";
import CommentForm from "../../forms/comment-form/CommentForm";
import CommentsList from "../comments-list/CommentsList";

const SectionComments = ({ post, onAdd }) => {
  const [ postComments, setPostComments ] = useState(post.comments ? [ ...post.comments ] : []);

  const addComment = comment => {
    setPostComments(prevState => post.comments = [ ...prevState, comment ]);
    onAdd();
  };

  // eslint-disable-next-line no-unused-vars
  const editComment = (updatedComment) => {
    setPostComments(prevState => prevState.map(curComment => curComment.id !== updatedComment.id ? curComment : updatedComment));
  };

  return (
    <section className="section-comments">
      <CommentForm
        post={post}
        comment={post}
        onAdd={addComment}
        onEdit={editComment}
      />
      <CommentsList
        comments={postComments}
        post={post}
        onDelete={() => {
        }}
        onEdit={editComment}
      />
    </section>
  );
};

export default SectionComments;
