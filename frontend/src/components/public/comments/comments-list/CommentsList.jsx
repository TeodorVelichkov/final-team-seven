import "./CommentsList.css";
import Comment from "../comment/Comment";

/*
COMMENT
{
  author: {id: 2, username: 'krisko', avatar: 'default.png'}
  content: "comment 3 and two deleted"
  createdOn: "Fri Nov 26 2021 13:14:35 GMT+0200 (Eastern European Standard Time)"
  embed: null
  id: 4
  likes: []
  picture: null
  updatedOn: "Fri Nov 26 2021 13:23:55 GMT+0200 (Eastern European Standard Time)"
}
 */

const CommentsList = ({ comments, post, onAdd, onEdit, onDelete }) => {

  return (
    <>
      {comments && <ul className="comments-list">
        {comments.map(curComment => <Comment key={curComment.id}
                                             comment={curComment}
                                             post={post}
                                             onEdit={onEdit}
                                             onAdd={onAdd}
                                             onDelete={onDelete}
        />)}
      </ul>}
    </>
  );
};

export default CommentsList;
