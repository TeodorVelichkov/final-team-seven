import { useState, useRef, useContext, useEffect } from "react";
import "./PostForm.css";
import { MdAddPhotoAlternate } from "react-icons/all";
import { GrClose } from "react-icons/gr";
import { FaWindowClose } from "react-icons/fa";
import { FiYoutube } from "react-icons/fi";
import PostsContext from "../../../../contexts/posts.context";
import useFileUpload from "../../../../hooks/useFileUpload";
import useInputChange from "../../../../hooks/useInputChange";
import Modal from "../../../../common/modal/Modal";
import Input from "../../../../common/input/Input";
import { APP_URL } from "../../../../common/Constants";
import { getAuthToken } from "../../../../services/AuthService";

const PostForm = ({ onClose, postToBeEdited }) => {
  const { addPost, editPost } = useContext(PostsContext);
  const { uploadFileHandler, hasImg, imgURL, setHasImg } = useFileUpload();
  const [ content, contentChangeHandler ] = useInputChange(!postToBeEdited ? "" : postToBeEdited.content);
  const [ isPublic, postTypeChangeHandler ] = useInputChange(!postToBeEdited ? 1 : !!postToBeEdited.isPublic);
  // eslint-disable-next-line no-unused-vars
  const [ youTubeLink, setYouTubeLink ] = useState("");
  const [ isEmbeddedShown, setIsEmbeddedShown ] = useState(false);

  const imgContainerRef = useRef();

  useEffect(() => {
    if (postToBeEdited && postToBeEdited.picture) {
      setHasImg(true);
    }
  }, []);

  const removeImg = () => setHasImg(false);

  const submitHandler = async evt => {
    evt.preventDefault();

    const formData = new FormData(evt.target);

    for (const formDatum of formData.entries()) {
      console.log(formDatum[0] + " " + formDatum[1]);
    }

    if (!hasImg) formData.delete("file");

    const createPostApi = async () => {
      return await fetch(`${APP_URL}/posts`, {
        method: "POST",
        body: formData,
        headers: {
          "Authorization": `Bearer ${getAuthToken()}`
        }
      })
        .then(res => res.json());
    };

    const editPostApi = async () => {
      return await fetch(`http://localhost:5000/posts/${postToBeEdited.id}`, {
        method: "PUT",
        body: formData,
        headers: {
          "Authorization": `Bearer ${getAuthToken()}`
        }
      })
        .then(res => res.json());
    };

    const formSubmit = async (addRequest, editRequest) => {
      let item;

      if (!postToBeEdited) {
        item = await addRequest();
        addPost(item);
      } else {
        item = await editRequest();
        editPost(item);
      }

      onClose();
    };

    await formSubmit(createPostApi, editPostApi);
  };

  const toggleEmbedField = () => {
    setIsEmbeddedShown(!isEmbeddedShown);
  };

  return (
    <Modal onClose={onClose}>
      <section className="section-post-form">
        <div className="section-post-form__heading">
          <h3 className="text-center">{!postToBeEdited ? "Create Post" : "Edit Post"}</h3>
          <GrClose onClick={onClose} className="cta--close-form cta--close-form--post"/>
        </div>
        <form onSubmit={submitHandler} className="post-form">
          <div className="post-actions">
            <Input input={{
              onChange: postTypeChangeHandler,
              id: "isPublic",
              name: "isPublic",
              component: "select",
              value: isPublic,
            }}
            >
              <option value="true">Public</option>
              <option value="false">Private</option>
            </Input>
            <label htmlFor="inputFile" className="cta--input-file-label cta--input-file-label--post">
              <MdAddPhotoAlternate className="upload-icon"/>
            </label>
            <input
              onChange={uploadFileHandler}
              type="file"
              accept="Image/*"
              id="inputFile"
              name="file"
            />
          </div>
          <Input input={{
            onChange: contentChangeHandler,
            id: "post",
            name: "content",
            component: "textarea",
            min: 2,
            rows: 4,
            autoFocus: true,
            value: content,
          }}
          />
          <label className="cta cta--embed cta--embed-post" onClick={toggleEmbedField} htmlFor="embed">
            <FiYoutube/>
          </label>
          {isEmbeddedShown &&
          <input
            onChange={evt => setYouTubeLink(evt.target.value.trim())}
            value={youTubeLink}
            name="embed"
            type="text"
            id="embed"
            placeholder="Link"
          />}
          {hasImg &&
          <div className="preview-img-box">
            <FaWindowClose onClick={removeImg} className="cta--remove-img"/>
            <img
              ref={imgContainerRef}
              src={imgURL ? imgURL : `http://localhost:5000/${postToBeEdited.picture}`}
              alt="Preview Img"
            />
          </div>}
          <button type="submit" className="cta--create-post">{!postToBeEdited ? "Post" : "Edit"}</button>
        </form>
      </section>
    </Modal>

  );

};

export default PostForm;
