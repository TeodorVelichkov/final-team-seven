import { useState } from "react";
import "./ItemFormSection.css";

/*{
  "content": "string", - DONE
  "embed": "string",
  "latitude": "string",
  "longitude": "string",
  "isPublic": "string" - DONE
}*/

const ItemFormSection = ({ onClose, onAdd, onEdit, item, itemToBeEdited, Form }) => {

  const [ hasImg, setHasImg ] = useState(itemToBeEdited);
  const [ imgURL, setImgURL ] = useState(!itemToBeEdited ? "" : `http://localhost:5000/${itemToBeEdited.picture}`);

  const uploadFileHandler = evt => {
    const file = evt.target.files[0];

    if (file) {
      setHasImg(true);
      const reader = new FileReader();

      reader.addEventListener("load", readerEvt => {
        setImgURL(readerEvt.target.result);
      });

      reader.readAsDataURL(file);
    }
  };

  const formSubmit = async (addRequest, editRequest) => {
    let item = null;

    if (!itemToBeEdited) {
      item = await addRequest();

      onAdd(item);
    } else {
      item = await editRequest();

      onEdit(item);
    }
  };

  return <Form
    onClose={onClose}
    onAdd={onAdd}
    onEdit={onEdit}
    onSubmit={formSubmit}
    hasImg={hasImg}
    imgURL={imgURL}
    onUpload={uploadFileHandler}
    itemToBeEdited={itemToBeEdited}
    post={item}
  />
};

export default ItemFormSection;
