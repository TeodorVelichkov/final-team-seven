import { useRef, useEffect, useState } from "react";
import "./CommentForm.css";
import { MdAddPhotoAlternate } from "react-icons/all";
import useFileUpload from "../../../../hooks/useFileUpload";
import useInputChange from "../../../../hooks/useInputChange";
import Input from "../../../../common/input/Input";
import { APP_URL } from "../../../../common/Constants";
import { getAuthToken } from "../../../../services/AuthService";
import { FiYoutube } from "react-icons/fi";

// eslint-disable-next-line no-unused-vars
const CommentForm = ({ onAdd, onEdit, post, commentToBeEdited, onCancel }) => {

  const { uploadFileHandler, hasImg, imgURL, setHasImg } = useFileUpload();
  const [ content, contentChangeHandler ] = useInputChange(!commentToBeEdited ? "" : commentToBeEdited.content);
  const [ youTubeLink, setYouTubeLink ] = useState("");
  const [ isEmbeddedShown, setIsEmbeddedShown ] = useState(false);

  const imgContainerRef = useRef();

  useEffect(() => {
    if (commentToBeEdited && commentToBeEdited.picture) {
      setHasImg(true);
    }
  }, []);


  const submitHandler = async evt => {
    evt.preventDefault();

    const formData = new FormData(evt.target);

    const createCommentApi = async () => {
      return await fetch(`http://localhost:5000/posts/${post.id}/comments`, {
        method: "POST",
        body: formData,
        headers: {
          "Authorization": `Bearer ${getAuthToken()}`
        }
      })
        .then(res => res.json());
    };

    const editCommentApi = async () => {
      return await fetch(`${APP_URL}/comments/${commentToBeEdited.id}`, {
        method: "PUT",
        body: formData,
        headers: {
          "Authorization": `Bearer ${getAuthToken()}`
        }
      })
        .then(res => res.json());
    };

    const formSubmit = async (addRequest, editRequest) => {
      let item = null;

      if (!commentToBeEdited) {
        item = await addRequest();
        onAdd(item);
      } else {
        item = await editRequest();
        onEdit(item);
        onCancel();
      }

      setHasImg(false);
    };

    await formSubmit(createCommentApi, editCommentApi);
  };

  const toggleEmbedField = () => {
    setIsEmbeddedShown(!isEmbeddedShown);
  };


  return (
    <section className="comment-form-box--real">
      <div className="comment-form-box__avatar"/>

      <form onSubmit={submitHandler} className="comment-form">
        <Input input={{
          onChange: contentChangeHandler,
          name: "content",
          component: "textarea",
          value: content,
          className: "comment-input",
          placeholder: "Add comment...",
          autoCorrect: "off",
          autoComplete: "off",
        }}
        />
        <label className="cta cta--embed cta--embed-comment" onClick={toggleEmbedField} htmlFor="embed-comment">
          <FiYoutube/>
        </label>
        {isEmbeddedShown &&
        <input
          onChange={evt => setYouTubeLink(evt.target.value.trim())}
          value={youTubeLink}
          name="embed"
          type="text"
          id="embed-comment"
          placeholder="Link"
        />}
        <label className="cta--input-file-label cta--input-file-label--comment">
          <MdAddPhotoAlternate className="upload-icon"/>
          <input
            onChange={uploadFileHandler}
            type="file"
            accept="Image/*"
            name="file"
          />
        </label>
        {hasImg && <div className="preview-comment-img-box">
          <img ref={imgContainerRef} src={imgURL ? imgURL : `${APP_URL}/${commentToBeEdited.picture}`}
               alt="Preview Img"/>
        </div>}
        <button type="submit" className="cta--create-post">{!commentToBeEdited ? "Add" : "Edit"}</button>
        {commentToBeEdited && <button onClick={onCancel} className="cta--cancel-comment">Cancel</button>}
      </form>
    </section>
  );
};

export default CommentForm;
