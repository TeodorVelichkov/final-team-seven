import React, { useContext, useEffect, useState } from "react";
import Button from "../../../common/button/Button";
import PostCard from "../post-card/PostCard";
import DeleteModal from "../delete/Delete";
import PostForm from "../forms/post-form/PostForm";
import { deletePost } from "../../../services/fetch-service";
import { getAuthToken } from "../../../services/AuthService";
import usePosts from "../../../hooks/usePosts";
import PostsContext from "../../../contexts/posts.context";
import AuthContext from "../../../contexts/auth.context";
import {USER_ROLES} from "../../../common/Constants";

const PostsComponent = ({ records, writable = true, isInSingleView }) => {

  // eslint-disable-next-line no-unused-vars
  const { posts, addPosts, addPost, editPost, isPostFormShown, setIsPostFormShown } = usePosts();
  const [ isDeleting, setIsDeleting ] = useState(false);
  const [ postId, setPostId ] = useState(null);
  const [ postToBeEdited, setPostToBeEdited ] = useState(null);
  const { userAuth } = useContext(AuthContext);

  useEffect(() => {
    if (!Array.isArray(records)) {
      addPost(records);

      return;
    }
    addPosts(records);
  }, []);

  const commentInputClickHandler = () => {
    setIsPostFormShown(true);
    setPostToBeEdited(null);
  };

  const closePostForm = () => {
    setIsPostFormShown(false);
  };

  const editMenuButtonClickHandler = post => {
    setPostToBeEdited(post);
    setIsPostFormShown(true);
  };

  const deletePostHandler = curPostId => {
    setIsDeleting(true);
    setPostId(curPostId);
  };

  const deletePostFinally = () => {
    deletePost(`/posts/${postId}`, undefined, getAuthToken());
    const result = posts.filter(curFeed => curFeed.id !== postId);
    addPosts(result);
    setIsDeleting(false);
  };

  return (
    <>
      <PostsContext.Provider value={{ posts, addPosts, addPost, editPost }}>
        {writable && <section className="comment-form-box">
          {/*<div className="comment-form-box__avatar"/>*/}
          <Button button={{ onClick: commentInputClickHandler, className: "comment-form-box__text" }}>
            Say something...
          </Button>
        </section>}
        {
          posts.length !== 0 && posts.map(post =>
            <PostCard key={post.id}
                      post={post}
                      onEdit={editMenuButtonClickHandler}
                      onDelete={deletePostHandler}
                      editable={(post.author.id === userAuth.user.id || userAuth.user.role === USER_ROLES.ADMIN)}
                      isInSingleView={isInSingleView}
            />
          )
        }

        {isDeleting && <DeleteModal
          onClose={() => setIsDeleting(false)}
          onDeletePost={deletePostFinally}
        />}
        {isPostFormShown && <PostForm
          onClose={closePostForm}
          postToBeEdited={postToBeEdited}
        />}
      </PostsContext.Provider>
    </>
  );
};

export default PostsComponent;
