import Reaction from "../post-card/reaction/Reaction";
import Test from "../aniticons/test";
import { FaRegThumbsUp } from "react-icons/fa";

const ReactionsActions = ({ reactions, onReact }) => {
  return (
    <button className="cta cta--post-action cta__like cta--comment-like">
      <ul className="reactions-list">
        {reactions.map((curReaction, index) => {
          return <Reaction key={curReaction}
                           onClick={onReact}
                           reaction={index}
          >
            <Test baseClass={curReaction} otherClasses="aniticon-select"/>
          </Reaction>;
        })}
      </ul>
      <FaRegThumbsUp/> Like
    </button>
  );
};

export default ReactionsActions;