import Test from "../aniticons/test";

const ReactionsMade = ({ children, reactionsMade, reactions, countReactions }) => {

  return (
    <div className="post__sub-info">
      <span className="post__reactions">
              {
                reactionsMade.length &&
                reactionsMade
                  .filter(curLikes => curLikes.length !== 0)
                  .map((curLikes, index) => {
                    return <div key={index} className="aniticon-box">
                      <Test baseClass={reactions[curLikes[0].reaction - 1]} otherClasses="aniticon-base"/>
                    </div>;
                  })
              }
      </span>
      <span className="post__reactions--counter">{countReactions !== 0 ? countReactions : null}</span>
      {children}
    </div>
  );
};

export default ReactionsMade;