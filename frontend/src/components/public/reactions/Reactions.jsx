import Test from "../aniticons/test";
import Reaction from "../post-card/reaction/Reaction";
import { FaRegThumbsUp } from "react-icons/fa";
import useReactions from "../../../hooks/useReactions";
import { getPost, postReact } from "../../../services/fetch-service";
import { getAuthToken } from "../../../services/AuthService";

const Reactions = ({ post }) => {
  const { reactions, reactionsMade, react, countReactions } = useReactions();

  const reactionClickHandler = async reaction => {
    await postReact(`/posts/${post.id}/react`, { reaction: reaction + 1 }, getAuthToken());

    const { likes } = await getPost(`/posts/${post.id}`, undefined, getAuthToken());

    react(likes || []);
  };

  return (
    <>
      <div className="post__sub-info">
            <span className="post__reactions">
              {
                reactionsMade.length &&
                reactionsMade
                  .filter(curLikes => curLikes.length !== 0)
                  .map((curLikes, index) => {
                    return <div key={index} className="aniticon-box">
                      <Test baseClass={reactions[curLikes[0].reaction - 1]} otherClasses="aniticon-base"/>
                    </div>;
                  })
              }
            </span>
        <span className="post__reactions--counter">{countReactions}</span>
        {/*<span className="post__comments-count">{!commentsCounter ? 0 : commentsCounter} Comments</span>*/}
      </div>
      <div className="post__actions">

        <button className="cta cta--post-action cta__like">
          <ul className="reactions-list">
            {reactions.map((curReaction, index) => {
              return <Reaction key={curReaction}
                               onClick={reactionClickHandler}
                               reaction={index}
              >
                <Test baseClass={curReaction} otherClasses="aniticon-select"/>
              </Reaction>;
            })}
          </ul>
          <FaRegThumbsUp/> Like
        </button>
        {/*{!isInSingleView &&*/}
        {/*<button onClick={commentsClickHandler} className="cta cta--post-action cta__comments">*/}
        {/*  <FaComments/> Comments*/}
        {/*</button>}*/}
      </div>

    </>
  );
};

export default Reactions;