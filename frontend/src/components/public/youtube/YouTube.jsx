const YouTube = ({ link }) => {
  const test = link
    .split("/")
    .filter(cur => cur);

  const first = test[0] + test[1];
  const last = test[test.length - 1].split("watch?v=").join("");

  return (
    <section className="youtube-section">
      <iframe width="100%" height="315" src={`${first}/embed/${last}`} title="YouTube video player"
              frameBorder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen
      />
    </section>
  );
};

export default YouTube;