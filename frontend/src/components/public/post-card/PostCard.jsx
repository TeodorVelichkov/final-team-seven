import { useState, useEffect } from "react";
import "./PostCard.css";
import { FaComments } from "react-icons/fa";
import SectionComments from "../comments/section-comments/SectionComments";
import useReactions from "../../../hooks/useReactions";
import BaseItemCard from "../base-item-card/BaseItemCard";
import { getAuthToken } from "../../../services/AuthService";
import { getPost, postReact } from "../../../services/fetch-service";
import ReactionsMade from "../reactions/ReactionsMade";
import ReactionsActions from "../reactions/ReactionsActions";

/*
POST
{
  author: {id: 1, username: 'admin', avatar: null}
  comments: []
  content: "PRIVATE BE BACEEE!!!"
  createdOn: "Tue Nov 23 2021 22:21:59 GMT+0200 (Eastern European Standard Time)"
  embed: null
  id: 6
  isPublic: 0
  latitude: null
  likes: []
  longitude: null
  picture: null
  updatedOn: "Tue Nov 23 2021 22:21:59 GMT+0200 (Eastern...
}
*/

const PostCard = ({ post, onEdit, onDelete, test, editable = true, isInSingleView = false }) => {
  const [ isCommentsShown, setIsCommentsShown ] = useState(false);
  const { reactions, reactionsMade, react, countReactions } = useReactions();
  const [ commentsCounter, setCommentsCounter ] = useState(0);

  useEffect(() => {
    if (!post.likes) return;

    setCommentsCounter(post.comments.length);

    react(post.likes);
  }, []);

  const incrementCommentsCounter = () => setCommentsCounter(prevState => prevState + 1);
  // eslint-disable-next-line no-unused-vars
  const decrementCommentsCounter = () => setCommentsCounter(prevState => prevState - 1);
  const commentsClickHandler = () => setIsCommentsShown(!isCommentsShown);

  const reactionClickHandler = async reaction => {
    await postReact(`/posts/${post.id}/react`, { reaction: reaction + 1 }, getAuthToken());

    const { likes } = await getPost(`/posts/${post.id}`, undefined, getAuthToken());

    react(likes || []);
  };

  return (
    <>
      <div className="post">
        <BaseItemCard
          item={post}
          onEdit={onEdit}
          onDelete={onDelete}
          editable={editable}
          isInSingleView={isInSingleView}
        />
        <div className="post__footer">
          <ReactionsMade
            reactions={reactions}
            reactionsMade={reactionsMade}
            countReactions={countReactions}
          >
            <span className="post__comments-count">{!commentsCounter ? 0 : commentsCounter} Comments</span>
          </ReactionsMade>
          <div className="post__actions">
            <ReactionsActions
              reactions={reactions}
              onReact={reactionClickHandler}
            />
            {!isInSingleView &&
            <button onClick={commentsClickHandler} className="cta cta--post-action cta__comments">
              <FaComments/> Comments
            </button>}
          </div>
        </div>
      </div>
      {isCommentsShown && <SectionComments post={post} onAdd={incrementCommentsCounter}/>}
      {test && <SectionComments post={post} onAdd={incrementCommentsCounter}/>}
      {isInSingleView && <SectionComments post={post} onAdd={incrementCommentsCounter}/>}
    </>
  );
};

export default PostCard;
