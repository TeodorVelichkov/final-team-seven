import "./Reaction.css";

const Reaction = ({ children, onClick, reaction }) => {

  const clickHandler = () => {
    onClick(reaction);
  };

  return (
    <span onClick={clickHandler} className="reaction">
      {children}
    </span>
  );
};

export default Reaction;
