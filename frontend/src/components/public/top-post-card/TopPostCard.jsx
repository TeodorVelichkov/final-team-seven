import "./TopPostCard.css";
import { Link } from "react-router-dom";

const TopPostCard = ({ post }) => {

  const shortenContent = content => {
    if (content.length > 50) return content.slice(0, 50).concat("...");
    return content;
  };

  return (
    <Link to={`/posts/${post.id}`} className="top-post">
      <div className="top-post__wrapper">
        <div className="">
          <div>
            {shortenContent(post.content ?? '')}
          </div>
        </div>
        <div className="post__sub-content">
          Created: {new Date(post.createdOn).toLocaleDateString()}
        </div>
      </div>
    </Link>
  );
};

export default TopPostCard;
