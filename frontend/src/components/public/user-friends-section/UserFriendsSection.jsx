import { useState, useEffect } from "react";
import "./UserFriendsSection.css";
// import { getUser } from "../../../services/fetch-service";
import { getAuthToken } from "../../../services/AuthService";
import jwtDecode from "jwt-decode";
import { APP_URL } from "../../../common/Constants";
import {NavLink} from "react-router-dom";

const UserFriendsSection = () => {
  // eslint-disable-next-line no-unused-vars
  const [ friends, setFriends ] = useState([]);

  const { id } = jwtDecode(getAuthToken());

  useEffect(() => {
    fetch(`${APP_URL}/users/${id}`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${getAuthToken()}`
      }
    })
      .then(res => res.json())
      .then(({ friends }) => {
        setFriends(() => friends);
      });
  }, []);

  return (
    <section className="section-friends">
      <h3 className="section-friends__heading">Friends</h3>
      <ul className="friends">
        {friends.map(({ id, avatar, username }) =>
          <li key={id} className="friends__item">
            <NavLink to={`/user/${id}`} className="friends__item--link">
              <span className="friends__item--avatar-box">
                <img src={`${APP_URL}/${avatar}`} alt=""/>
              </span>
              <span className="friend-username">{username}</span>
            </NavLink>
          </li>)}
      </ul>
    </section>
  );
};

export default UserFriendsSection;
