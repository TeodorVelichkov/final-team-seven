import Styled from "styled-components";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export const Icon = Styled(FontAwesomeIcon)`
    color: ${ props => props.color || "inherit" };
    font-size: ${ props => props.size };
    cursor: pointer;
`;
