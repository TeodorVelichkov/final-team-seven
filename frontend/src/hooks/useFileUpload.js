import { useState } from "react";

const useFileUpload = () => {
  const [ hasImg, setHasImg ] = useState(false);
  const [ imgURL, setImgURL ] = useState(null);
  // let imgURL = null;

  const uploadFileHandler = evt => {
    const file = evt.target.files[0];

    if (file) {
      const reader = new FileReader();

      reader.addEventListener("load", readerEvt => {
        setImgURL(readerEvt.target.result);
        // imgURL = readerEvt.target.result;
        setHasImg(true);
      });

      reader.readAsDataURL(file);
    }
  };

  return {
    uploadFileHandler,
    hasImg,
    imgURL,
    setHasImg,
  }
}

export default useFileUpload;