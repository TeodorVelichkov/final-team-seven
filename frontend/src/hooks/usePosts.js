import { useState } from "react";

const usePosts = () => {
  const [ posts, setPosts ] = useState([]);
  const [ isPostFormShown, setIsPostFormShown ] = useState(false);
  const [ isPostInEditMode, setIsPostOnEditMode ] = useState(false);

  const addPost = post => {
    setPosts(prevState => [ post, ...prevState ]);
    // setIsPostFormShown(!isPostFormShown);
  };

  const addPosts = posts => setPosts(() => [ ...posts ]);

  const editPost = post => {
    const result = posts.map(curPost => {
      if (curPost.id === post.id) return post;

      return curPost;
    });

    addPosts(result);
    setIsPostFormShown(false);
  };

  return {
    posts,
    addPost,
    addPosts,
    editPost,
    isPostFormShown,
    setIsPostFormShown,
    isPostInEditMode,
    setIsPostOnEditMode,
  };
};

export default usePosts;
