import { useState } from "react";

const reactions = [ "blink", "sad", "happy", "ninja", "cat", "angry" ];

const useReactions = () => {
  const [ reactionsMade, setReactionsMade ] = useState([]);

  const addReactions = reactions => setReactionsMade(() => [ ...reactions ]);

  const react = likes => {
    const result = likes.reduce((acc, curLike) => {
      acc[curLike.reaction - 1].push(curLike);

      return acc;
    }, Array.from({ length: reactions.length }, () => []));

    setReactionsMade(() => result);
  };

  const countReactions = reactionsMade.reduce((counter, curReactions) => counter += curReactions.length, 0);

  return {
    reactions,
    reactionsMade,
    addReactions,
    react,
    countReactions,
  };
};

export default useReactions;
