import { useState } from "react";

const useForm = submitCallback => {
  const [ state, setState ] = useState({});

  const handleSubmit = event => {
    event.preventDefault();
    submitCallback(event);
  };

  const handleChange = event => {
    event.persist();
    setState(previous => ({ ...previous, [event.target.name]: event.target.value }) );
  };

  return [ state, handleChange, handleSubmit ];
};

export default useForm;
