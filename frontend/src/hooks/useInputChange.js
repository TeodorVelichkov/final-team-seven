import { useState } from "react";

const useInputChange = (initVal = "") => {
    const [enteredValue, setEnteredValue] = useState(initVal);

    const changeHandler = evt => {
        setEnteredValue(evt.target.value);
    }

    return [enteredValue, changeHandler];
}

export default useInputChange;