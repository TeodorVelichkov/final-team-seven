import { useState } from "react";
import AuthContext, { AuthDefaults } from "./contexts/auth.context";
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import HomeView from "./views/public/HomeView/HomeView";
import AuthenticationView from "./views/common/AuthenticationView/AuthenticationView";
import PrivateRoute from "./common/authentication/PrivateRoute";
import {USER_ROLES} from "./common/Constants";
import NotificationContext from "./contexts/notification.context";
import NotificationComponent from "./common/notification/NotificationComponent";
import UserView from "./views/public/UserView/UserView";
import PublicRoute from "./common/authentication/PublicRoute";
import SinglePostView from "./views/public/SinglePostView/SinglePostView";
import AdminView from "./views/admin/AdminView/AdminView";

function App() {
  const [userAuth, updateUserAuth] = useState(AuthDefaults);
  const [ notification, setNotification ] = useState( { active: false, message: '', color: '' });

  return (
      <AuthContext.Provider value={{ userAuth, updateUserAuth }}>
          <NotificationContext.Provider value={ { setNotification } }>
            <Router>
              <Switch>
                  <PrivateRoute exact path={`/`} roles={[ USER_ROLES.USER, USER_ROLES.ADMIN ]} redirect={`/authentication`} component={ HomeView }/>
                  <PrivateRoute exact path={`/profile`} roles={[ USER_ROLES.USER, USER_ROLES.ADMIN ]} redirect={`/authentication`} component={ UserView }/>
                  <PrivateRoute exact path={`/user/:id`} roles={[ USER_ROLES.USER, USER_ROLES.ADMIN ]} redirect={`/authentication`} component={ UserView }/>
                <PrivateRoute exact path={`/posts/:id`} roles={[ USER_ROLES.USER, USER_ROLES.ADMIN ]} redirect={`/authentication`} component={ SinglePostView }/>
                  <PrivateRoute path={`/admin`} roles={[ USER_ROLES.USER, USER_ROLES.ADMIN ]} redirect={`/`} component={ AdminView }/>
                  <PublicRoute path="/authentication" component={ AuthenticationView } redirect={`/`}/>
              </Switch>
            </Router>
            <NotificationComponent setNotification={setNotification}
                                   active={notification.active}
                                   message={notification.message}
                                   color={notification.color}/>
          </NotificationContext.Provider>
      </AuthContext.Provider>
  );
}

export default App;
