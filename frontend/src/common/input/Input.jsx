const Input = ({input, children}) => {
  return (
    <div className="form-field">
      <label htmlFor={input.id}>{input.label}</label>
      {input.component === "input" ? <input.component {...input}/> : <input.component {...input}>{children}</input.component>}
    </div>
  );
};

export default Input;