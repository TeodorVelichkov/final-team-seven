import "./Modal.css";

// eslint-disable-next-line no-unused-vars
const Modal = ({children, onClose}) => {

    return (
        <div className="module">
            <div onClick={onClose} className="backdrop"/>
            <div className="module-layout">
                {children}
            </div>
        </div>
    )
}

export default Modal;
