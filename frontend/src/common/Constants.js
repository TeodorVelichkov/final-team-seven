export const APP_URL = "http://localhost:5000";
export const PLAIN_SERVER_URL = "http://localhost:5000/";

export const DEVICE = {
  small:"550px",
  medium:"768px",
  large:"1024px"
};

export const LAYOUT = {
  navigationHeight: '6rem'
};

export const COLORS = {
  navy:"rgba(162,200,236, 1.0)",
  orangeColor: "rgba(254, 124, 49, 1.0)",
  lightBlack: "rgba(68, 68, 68, 1)",
  lightBlue: "rgba(66, 135, 245, 1.0)",
  danger: "rgba(220, 53, 69, 1)",
  success: "rgba(40, 167, 69, 1)",
  primary: "rgba(0, 123, 255, 1)",
  lightRed: '#e60023',
  brownDark: "rgba(216, 125, 74, 1.0)",
  brownLight:"rgba(251,175,133,1.0)",
  grayMid:"rgba(207, 207, 207,1.0)",
  grayDark:"rgba(241,241,241,1.0)",
  grayLight:"rgba(250,250,250,1.0)",
  grayTransparent:"rgba(0, 0, 0, .3)",
  white:"rgba(255,255,255, 1.0)",
  blackLight:"rgba(16,16,16, .4)",
  blackStupid:"rgba(25,25,25,1.0)",
  blackStupid2:"rgba(79,79,79,1.0)",
  black:"rgba(0,0,0,1.0)",
};

export const USER_ROLES = {
  USER: 1,
  ADMIN: 2
};

export const POST_VISIBILITIES = {
  ['PUBLIC']: 0,
  ['PRIVATE']: 1
};

export const POST_STATES = {
  DELETED: 0,
  AVAILABLE: 1
};
