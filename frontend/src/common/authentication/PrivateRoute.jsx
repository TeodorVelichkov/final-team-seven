import React from "react";
import { Redirect, Route } from "react-router-dom";
import { checkUserRole } from "../../services/AuthService";

// eslint-disable-next-line no-unused-vars
const PrivateRoute = ({ component: Component, roles, redirect, ...rest }) => {
  const passed = checkUserRole( roles );

  return (
    <Route
      {...rest} render={(props) => (
      passed
        ? <Component {...props} />
        : <Redirect to={redirect} />
    )}
    />
  );
};

export default PrivateRoute;
