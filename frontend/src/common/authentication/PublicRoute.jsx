import { Redirect, Route } from "react-router-dom";
import {checkIsUserAuthenticated} from "../../services/AuthService";
import React from "react";

const PublicRoute = ({ component: Component, redirect, ...rest }) => {
    const passed = checkIsUserAuthenticated();

    console.log(passed)
    return (
        <Route
            {...rest} render={(props) => (
            passed !== true
                ? <Component {...props} />
                : <Redirect to={redirect} />
        )}
        />
    );
}

export default PublicRoute;
