import Styled from "styled-components";
import { COLORS } from "../Constants";

export const NotificationWrapper = Styled.div`
    position: fixed;
    top: 10%;
    right: 0;
    min-width: 20rem;
    max-width: max-content;
    transform: translateX(100%);
    transition: transform .5s ease-in-out;
    font-size: 1.8rem;
    color: ${ COLORS.white };
    background-color: ${ props => props.color };
    border-radius: 5px;
    z-index: 1000;
    
    &.active {
       transform: translateX(0);
       transition: transform .5s ease-in-out;
    }
`;

export const NotificationContainer = Styled.div`
     display: flex;
     justify-content: space-between;
     align-items: center;
     padding: .6rem 1.2rem .6rem 1.4rem;
     
     & span {
      padding-right: 1rem;
     }
`;
