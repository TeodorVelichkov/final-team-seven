import React, { useEffect } from "react";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { COLORS } from "../Constants";
import { NotificationContainer, NotificationWrapper } from "./NotificationStyling";
import { Icon } from "../../styles/genericStyledComponents/generic";

const NotificationComponent = ({ message, color, active, setNotification }) => {

  useEffect(() => {
    setTimeout(() => setNotification({ message: "", color: "", active: false }), 2500);
  }, [active]);

  return (
    <NotificationWrapper color={ color } className={ active ? "active" : "" }>
      <NotificationContainer>
        <span> { message } </span>
        <Icon color={ COLORS.white } onClick={() => setNotification({ message: "", color: "", active: false }) } icon={ faTimes }/>
      </NotificationContainer>
    </NotificationWrapper>
  );
};

export default NotificationComponent;
