import { createContext } from "react";
import {checkIsUserAuthenticated, getUserAvatar, getUserId, getUserName, getUserRole} from "../services/AuthService";

export const AuthDefaults = {
  isAuthenticated: checkIsUserAuthenticated(),
  user: {
    id: getUserId(),
    role: getUserRole(),
    username: getUserName(),
    avatar: getUserAvatar()
  },
};

const AuthContext = createContext(AuthDefaults);

export default AuthContext;

