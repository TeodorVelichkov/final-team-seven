import { createContext } from "react";

const NotificationContext = createContext({
  active: true,
  message: "",
  color: ""
});

export default NotificationContext;
