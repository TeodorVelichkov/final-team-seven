import { createContext } from "react";

export const AuthViewDefaults = {
    columns: 5,
    postsPerColumn: 4,
};

const AuthViewContext = createContext(AuthViewDefaults);

export default AuthViewContext;
