import styled from "styled-components";
import Styled from "styled-components";
import {Icon} from "../../../styles/genericStyledComponents/generic";

export const AuthenticationWrapper = styled.div`
  position: relative;
  display: grid;
  grid-template-rows: 8.5rem 15rem calc(200vh - 27.5rem) 4rem;
  width: 100%;
  min-height: 100vh;
  height: max-content;
`;

export const AuthModalContainer = styled.div`
  position: absolute;
  top: 50%;
  left: 0;
  width: 100%;
  height: 100vh;
  display: grid;
  place-items: center;
  background-color: rgba(0, 0, 0, .1);
`;

export const DisplayContainer = styled.div`
  & > .module {
    position: relative !important;
    width: 100%;
    height: 100%;
  }
`;

export const HeroIntroContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`;

export const HeroIntroHeading = styled.h1`
  font-size: 6rem;
  font-weight: 700;
  text-transform: uppercase;
`;

export const MovingHeadingContainer = styled.div`
  position: relative;
  width: 55rem;
  height: 6rem;
  overflow: hidden;
  font-size: 4rem;
  font-weight: 600;
`;

export const MovingHeading = styled.p`
  position: absolute;
  top: 100%;
  left: 50%;
  width: max-content;
  transform: translateX(-50%);
  opacity: 0;
  color: ${ props => props.color || 'red' };
  transition: top .5s ease-in-out, opacity .2s ease-in-out;
  
  &.active {
    top: 0;
    opacity: 100%;
  }
`;

export const IntroContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  height: 100%;
  overflow: hidden;
`;

export const PopularPostsContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(${ props => props.columns || 3 }, 1fr);
  width: 100%;
  max-width: 125rem;
  height: 100%;
`;

export const AuthFooter = styled.footer`
  position: relative;
  z-index: 100;
  background-color: aliceblue;
`;

export const AuthNavigationModal = styled.div`
  position: absolute;
  top: calc(50% - 20rem);
  left: 0;
  display: flex;
  justify-content: center;
  align-items: flex-start;
  width: 100%;
  height: 20rem;
  transform: translateY(50%);
  transition: opacity .2s .3s ease-in-out;
  background: linear-gradient(0deg, transparent 0%, rgba(255, 255, 255, .8) 50%, rgba(255, 255, 255, .8) 50%, transparent 100%);
  
  &.active {
    opacity: 0;
    transition: opacity .2s ease-in-out;
  }
`;

export const ChevroletIcon = Styled(Icon)`
   font-size: 5.5rem;
   animation: bounce 1s infinite alternate;
    
   @keyframes bounce {
      from {
        transform: translateY(0px);
      }
      to {
        transform: translateY(2rem);
      }
}
`
