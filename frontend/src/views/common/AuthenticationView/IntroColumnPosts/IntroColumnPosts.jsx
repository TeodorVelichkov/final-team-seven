import React, {useContext, useEffect, useState} from 'react';
import AuthViewContext from "../../../../contexts/auth.view.context";
import IntroPostCard from "../IntroPostCard/IntroPostCard";
import {PostsContainer} from "./IntroColumsPostsStyles";

const IntroColumnPosts = ({ count, allPosts }) => {

    const { defaults: { postsPerColumn, columns } } = useContext(AuthViewContext);
    const middle = Math.ceil( columns / 2);
    const [ start, end ] = [ count * postsPerColumn, (count * postsPerColumn) + postsPerColumn ];
    const [ posts, setPosts ] = useState([]);

    useEffect(() => {
        setPosts( [ ...allPosts.slice(start, end) ]);
    }, []);

    return (
        <PostsContainer number={ count < middle ? count : columns - count - 1 }>
            { posts.map( post => <IntroPostCard key={ post.id } post={ post }/> ) }
        </PostsContainer>
    );
};

export default IntroColumnPosts;
