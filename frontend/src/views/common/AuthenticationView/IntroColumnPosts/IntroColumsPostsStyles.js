import styled from "styled-components";

export const PostsContainer = styled.div`
  margin-top: calc( ${ props => props.number } * 7.5rem);
  
  & + & {
    margin-left: 2.5rem;
  }
  
`
