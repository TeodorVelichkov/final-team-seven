import React, {useEffect, useState } from 'react';
import {
    AuthenticationWrapper,
    AuthModalContainer, AuthNavigationModal, ChevroletIcon, DisplayContainer,
    HeroIntroContainer, HeroIntroHeading,
    IntroContainer, MovingHeading, MovingHeadingContainer,
    PopularPostsContainer
} from "./AuthenticationStyles";
import {getMostPopularPost} from "../../../services/CommonService";
import IntroColumnPosts from "./IntroColumnPosts/IntroColumnPosts";
import AuthViewContext from "../../../contexts/auth.view.context";
import useWindowSize from "../../../hooks/UseWindowSize";
import { faChevronCircleDown } from "@fortawesome/free-solid-svg-icons";
import NavigationAuthComponent from "../../../components/public/navigation-auth/NavigationAuthComponent";
import Login from "../../../components/common/auth/login/Login";
import {withRouter} from "react-router-dom";
import Register from "../../../components/common/auth/register/Register";
import {FooterContainer, Wave, WavesContainer} from "../../../components/common/footer/FooterComponentStyles";

const AuthenticationView = () => {

    const [ popular, setPopular ] = useState([]);
    const [ defaults, updateDefaults ] = useState({  columns: 5, postsPerColumn: 4 });
    const [ isAuthModalOpen ] = useState(true);
    const [ activeHeading, setActiveHeading ] = useState( { active: 0, records: [ false, false, false ] });
    const [ activeAuth, setActiveAuth ] = useState( { register: false, login: true });
    const size = useWindowSize();

    const setPagePositionDown = () => {
        document.documentElement.scrollTop = document.body.scrollTop = window.innerHeight;
    }

    const openAuthLogin = () => {
        activeAuth.register = false;
        activeAuth.login = true;
        setPagePositionDown();
    }

    const openAuthRegister = () => {
        setPagePositionDown();
        activeAuth.login = false;
        activeAuth.register = true;
    }

    useEffect(() => {
        getMostPopularPost()
            .then(response => { setPopular(response); console.log(response) } )
            .catch(error => console.log( error ));

        const update = (record) => {
            let next = record.active + 1;
            record.records[next - 1] = false;

            if(next >= activeHeading.records.length) next = 0;
            record.records[next] = true;
            record.active = next;

            return record;
        }

        const interval = setInterval( () => {
            setActiveHeading((record) => ({ ...update(record) }) );
        }, 2500);

        return () => {
            clearInterval(interval);
        }
    }, []);

    useEffect(() => {
        if( size.width < 1200 && size.width > 800) updateDefaults({ ...defaults, columns: 4 });
        else if( size.width < 800 && size.width > 600) updateDefaults( { ...defaults, columns: 3 });
        else if( size.width < 600) updateDefaults( { ...defaults, columns: 2 });
        else updateDefaults( { ...defaults, columns: 5 });

    }, [ size ]);


    return (
        <AuthenticationWrapper>
            <AuthViewContext.Provider value={{ defaults, updateDefaults }}>
                <NavigationAuthComponent openAuthRegister={openAuthRegister} openAuthLogin={openAuthLogin} />
                <HeroIntroContainer>
                    <HeroIntroHeading> Reasons to Join </HeroIntroHeading>
                    <MovingHeadingContainer>
                        <MovingHeading className={ activeHeading.records[0] ? 'active' : '' } color={ 'green' }>  Content quality.  </MovingHeading>
                        <MovingHeading className={ activeHeading.records[1] ? 'active' : '' } color={ 'orange' }> Biggest anime community!  </MovingHeading>
                        <MovingHeading className={ activeHeading.records[2] ? 'active' : '' } color={ 'blue' }> It will be fun! </MovingHeading>
                    </MovingHeadingContainer>
                </HeroIntroContainer>
                <IntroContainer>
                    <PopularPostsContainer columns={ defaults.columns }>
                        { popular.length !== 0 &&
                             Array.from({ length: defaults.columns }).map(
                                 (_, index) => <IntroColumnPosts key={index} count={ index } allPosts={ popular }/>
                             )
                        }
                    </PopularPostsContainer>
                </IntroContainer>
                <FooterContainer>
                    <WavesContainer>
                        <Wave delay={'0s'} color={'linear-gradient(243deg, rgba(131,58,180,1) 0%, rgba(253,29,29,1) 50%, rgba(252,176,69,1) 100%);'}/>
                        <Wave delay={'5s'} color={'linear-gradient(270deg, rgba(34,193,195,1) 0%, rgba(253,187,45,1) 100%)'} />
                    </WavesContainer>
                </FooterContainer>
            </AuthViewContext.Provider>
            <AuthModalContainer className='active'>
                <DisplayContainer style={ {'display': (activeAuth.login ? 'block' : 'none')} }> <Login/> </DisplayContainer>
                <DisplayContainer style={ {'display': (activeAuth.register ? 'block' : 'none')} }> <Register/> </DisplayContainer>
            </AuthModalContainer>
            <AuthNavigationModal className={ !isAuthModalOpen ? 'active' : '' }>
                <ChevroletIcon onClick={() => { setPagePositionDown(); setActiveAuth({ register: false, login: true }); } }
                               color={'#407a57'} icon={faChevronCircleDown}/>
            </AuthNavigationModal>
        </AuthenticationWrapper>
    );
};

export default withRouter(AuthenticationView);
