import styled from "styled-components";

export const IntroCardBackgroundImage = styled.img`
  width: 100%;
  height: 100%;
  transition: all .3s ease-in-out;
  object-fit: cover;
`;

export const IntroCardWrapper = styled.div`
  position: relative;
  width: 100%;
  height: 35rem;
  border-radius: 1rem;
  overflow: hidden;
  cursor: pointer;
  
  & + & { margin-top: .5rem; }
  
  &:hover ${IntroCardBackgroundImage} {
    transform: scale(1.05);
    color: red;
  }
`;

export const ContentContainer = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  width: 100%;
  padding: .8rem .3rem;
  background: linear-gradient(0deg, transparent 0%, rgba(255, 255, 255, .8) 100%);
`;

export const IconsContainer = styled.div`
  display: flex;
  justify-content: flex-end;
`;
