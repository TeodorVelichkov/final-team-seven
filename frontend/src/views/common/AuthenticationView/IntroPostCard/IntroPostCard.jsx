import React from 'react';
import {ContentContainer, IntroCardBackgroundImage, IntroCardWrapper} from "./IntroPostCardStyles";
import { APP_URL } from "../../../../common/Constants";

const IntroPostCard = ({ post }) => {

    const shortenContent = content => {
        if (content.length > 35) return content.slice(0, 35).concat("...");
        return content;
    };

    return (
        <IntroCardWrapper>
            <IntroCardBackgroundImage src={ `${APP_URL}/${post.picture}`}/>
            <ContentContainer>
                <p> { shortenContent(post.content ?? '') }</p>
            </ContentContainer>
        </IntroCardWrapper>
    );
};

export default IntroPostCard;
