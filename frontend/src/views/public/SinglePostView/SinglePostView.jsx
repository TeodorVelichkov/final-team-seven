import React, { useEffect, useState } from "react";
import "./SinglePostView.css";
import { getPost } from "../../../services/fetch-service";
import { getAuthToken } from "../../../services/AuthService";
import { APP_URL } from "../../../common/Constants";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
// import PostCard from "../../../components/public/post-card/PostCard";
import Navigation from "../../../components/common/navigation/NavigationComponent";
import { NavigationContainer } from "../HomeView/HomeViewStyles";
import PostsComponent from "../../../components/public/posts/PostsComponent";

const SinglePostView = ({ match, id }) => {
  // eslint-disable-next-line no-unused-vars
  const [ singlePost, setSinglePost ] = useState(null);
  const postId = id === undefined ? match.params.id : id;

  useEffect(() => {
    getPost(`/posts/${postId}`, undefined, getAuthToken())
      .then(post => {
        console.log(post); setSinglePost(post) })
      .catch(e => {
        console.log(e);
      });
  }, []);

  return (
    <>
     { id === undefined &&
        <NavigationContainer>
            <Navigation/>
        </NavigationContainer> }
      <div className="single-post-page">
        <div className="single-post-page__left">
        { id === undefined && <Link to="/">
            <FontAwesomeIcon icon={faTimes} className="cta--close-single-post"/>
          </Link> }
          <div className="single-post-img-box">
            {singlePost && <img src={`${APP_URL}/${singlePost.picture}`} alt="post img"/>}
          </div>
        </div>
        <div className="single-post-page__right">
          {singlePost &&
          <PostsComponent
            records={[singlePost]}
            writable={false}
            isInSingleView={true}
          />}
        </div>
      </div>
    </>
  );
};

export default SinglePostView;
