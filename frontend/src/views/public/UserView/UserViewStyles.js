import styled from "styled-components";
import {COLORS, DEVICE, LAYOUT} from "../../../common/Constants";
import {Link} from "react-router-dom";

export const LayoutUserView = styled.div`
    display: grid;
    grid-template-rows: ${LAYOUT.navigationHeight} 1fr;
    width: 100%;
    min-height: 100%;
    max-height: max-content;
`;

export const UserViewContainer = styled.div`
    display: grid;
    grid-template-rows: 30rem 1fr;
    grid-column: 1/4;
    gap: 12rem;
`;

export const HeaderUserView = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;
  background-color: #636e72;
`;

export const UserInfoContainer = styled.form`
  position: relative;
  display: flex;
  align-items: center;
  width: min(85vw, 110rem);
  transform: translateY(50%);
  padding: 1.2rem 2.5rem;
  border-radius: 10px;
  border: 5px solid white;
  background-color: #5c83a5;
`;

export const UserPictureContainer = styled.div`
  position: relative;
  width: 13rem;
  height: 13rem;
  border-radius: 50%;
  overflow: hidden;

  @media (max-width: ${DEVICE.medium}) {

    & {
      width: 7.5rem;
      height: 7.5rem;
    }
  }
`;

export const UpdatePictureContainer = styled.div`
  position: absolute;
  right: 0;
  bottom: 0;
  height: 5rem;
  width: 100%;
  display: flex;
  justify-content: center;
  backdrop-filter: blur(4px);
`;

export const PictureInput = styled.input`
    display: none;
`;

export const LabelPictureInput = styled.label`
    display: block;
    width: max-content;
    height: max-content;
    margin-top: .5rem;
    font-size: 1.2rem;
    color: ${ COLORS.white };
    background-color: #5c83a5;
    padding: .5rem 1rem; 
    text-transform: uppercase;
    border-radius: 5px;
    cursor: pointer;
  
  @media (max-width: ${DEVICE.medium}) {
    & {
      font-size: .9rem;
      margin-top: .7rem;
    }
  }
  
    &:hover {
        color: #5c83a5;
        background-color: ${ COLORS.white };
    }
`;

export const UsernameInput = styled.input`
  width: 35rem;
  font-size: 3.5rem;
  border: none;
  color: ${ COLORS.blackStupid };
  margin-left: 1.5rem;
  background: transparent;

  @media (max-width: ${DEVICE.medium}) {

    & {
      width: 20rem;
    }
  }
  
  &:disabled {
    color: ${ COLORS.blackStupid };
  }
  
  &:focus-visible {
    outline: none;
  }
`;

export const AddFriendButton = styled.button`
  width: max-content;
  font-size: 1.2rem;
  font-weight: 600;
  text-transform: uppercase;
  border: .2rem solid white;
  border-radius: 1rem;
  cursor: pointer;
`;

export const UserUpdateButton = styled.button`
  position: absolute;
  top: 0;
  right: 5%;
  width: max-content;
  margin-left: auto;
  padding: .5rem 2.5rem;
  transform: translateY(-50%);
  font-size: 1.7rem;
  font-weight: 600;
  color: white;
  background-color: #5c83a5;
  border: .3rem solid white;
  border-radius: 1rem;
  cursor: pointer;
  
  &:hover {
    color: #5c83a5;
    background-color: white;
  }
`;

export const UserPicture = styled.img`
  object-fit: cover;
  width: 100%;
  height: 100%;
`;

export const FriendsUserContainer = styled.div`
  width: 50rem;
  margin: 0 auto;
`;

export const UserContentContainer = styled.div`
  display: grid;
  grid-template-columns: 60rem 1fr;
  padding: 0 1.3rem;

  @media (max-width: ${DEVICE.medium}) {

    & {
      grid-template-columns: 1fr;
      grid-template-rows: auto 1fr;
      gap: 3.5rem;
    }
  }
`;

export const FriendCardContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: flex-start;
  gap: 1.2rem;
  width: 100%;
  height: max-content;
  max-height: 60rem;
  overflow-y: auto;
`;

export const FriendCard = styled(Link)`
  position: relative;
  width: 15rem;
  height: 15rem;
  border-radius: 10px;
  overflow: hidden;
`;

export const FriendCardPicture = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const FriendUsername = styled.h4`
  position: absolute;
  left: 5%;
  bottom: 0;
  color: ${ COLORS.black };
`;

export const PostsUserContainer = styled.div`
  width: 100%;
  max-width: 50rem;
  margin: auto;
`;
