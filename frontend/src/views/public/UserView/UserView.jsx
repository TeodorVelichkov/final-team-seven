import React, {useContext, useEffect, useState} from 'react';
import {NavigationContainer} from "../HomeView/HomeViewStyles";
import Navigation from "../../../components/common/navigation/NavigationComponent";
import {
    UserViewContainer,
    HeaderUserView,
    LayoutUserView,
    PostsUserContainer,
    UserContentContainer,
    UserInfoContainer,
    UserPictureContainer,
    UserPicture,
    LabelPictureInput,
    PictureInput,
    UpdatePictureContainer,
    UsernameInput,
    UserUpdateButton,
    AddFriendButton,
    FriendsUserContainer,
    FriendCardContainer,
    FriendCard, FriendCardPicture, FriendUsername
} from "./UserViewStyles";
import AuthContext from "../../../contexts/auth.context";
import {
    toggleUserFriend,
    getUserDetails,
    getUserPosts,
    updateUserRequest
} from "../../../services/UserService";
import PostsComponent from "../../../components/public/posts/PostsComponent";
import {APP_URL, COLORS} from "../../../common/Constants";
import {Icon} from "../../../styles/genericStyledComponents/generic";
import {faUpload, faPlus} from "@fortawesome/free-solid-svg-icons";
import NotificationContext from "../../../contexts/notification.context";

const UserView = ({ match }) => {
    const [isLoading, setIsLoading] = useState(true);
    const [update, setUpdate] = useState(true);
    const [user, setUser] = useState({});
    const [username, setUsername] = useState({});
    const { userAuth } = useContext(AuthContext);
    const { setNotification } = useContext(NotificationContext);

    const permission = (match.params.id === undefined) || match.params.id === userAuth.user.id;
    const viewUserId = permission ? userAuth.user.id : match.params.id;

    useEffect(() => {
        Promise.all([getUserDetails(viewUserId), getUserPosts(viewUserId)])
            .then(([userDetails, posts]) => {
                setUser({ ...userDetails, posts });
                setUsername(userDetails.username);
                setIsLoading(false);
            })
            .catch(error => console.log(error));

        return () => { setIsLoading(true); };
    }, [ viewUserId, update ]);

    const updateUser = (event) => {
        event.preventDefault();
        updateUserRequest(event)
            .then(() => {
                setUpdate(!update);
                setNotification({ active: true, message: 'User updated successfully', color: COLORS.success })
            })
            .catch(error => console.log(error));
    }

    const addFriend = () => {
        toggleUserFriend(userAuth.user.id, viewUserId)
            .then(response => {
                setUpdate(!update);
                setNotification({ active: true, message: response.message, color: COLORS.success })
            })
            .catch(error => console.log(error));
    };

    const removeFriend = () => {
        toggleUserFriend(userAuth.user.id, viewUserId, 'DELETE')
            .then(response => {
                setUpdate(!update);
                setNotification({ active: true, message: response.message, color: COLORS.success })
            })
            .catch(error => console.log(error));
    }

    const checkIsAFriend = () => {
        return user.friends.find(friend => friend.id === userAuth.user.id) ?? false;
    };

    return (
        <LayoutUserView>
            <NavigationContainer>
                <Navigation/>
            </NavigationContainer>
            <UserViewContainer>
                <HeaderUserView>
                    { !isLoading && <UserInfoContainer onSubmit={event => updateUser(event)}>
                        <UserPictureContainer>
                            { !isLoading && <UserPicture src={`${APP_URL}/${user.avatar}` } alt={'user picture'}/> }
                            { permission && <UpdatePictureContainer>
                                <LabelPictureInput>
                                    <Icon icon={faUpload}/> upload
                                    <PictureInput name="file" type="file"/>
                                </LabelPictureInput>
                            </UpdatePictureContainer> }
                        </UserPictureContainer>
                        <UsernameInput onChange={({ target }) => setUsername(target.value)} name='username' value={ username } disabled={ !permission }/>
                        { (parseInt(viewUserId) !== userAuth.user.id && checkIsAFriend() ) &&
                            <AddFriendButton onClick={removeFriend} type={'button'}> Unfriend </AddFriendButton> }

                        { (parseInt(viewUserId) !== userAuth.user.id && !checkIsAFriend()) &&
                            <AddFriendButton onClick={addFriend} type={'button'}>
                                <Icon icon={faPlus}/> Add friend
                            </AddFriendButton> }
                        { permission && <UserUpdateButton type={'submit'}> Save </UserUpdateButton> }
                    </UserInfoContainer> }
                </HeaderUserView>
                { !isLoading && <UserContentContainer>
                    <FriendsUserContainer>
                        <h2> {`Friends (${ user.friends.length })`} </h2>
                        <FriendCardContainer>
                            { user.friends.map(friend =>
                                <FriendCard to={`/user/${friend.id}`} key={friend.id}>
                                    <FriendCardPicture src={`${APP_URL}/${friend.avatar}`} alt={'user picture'}/>
                                    <FriendUsername> { friend.username } </FriendUsername>
                                </FriendCard> ) }
                        </FriendCardContainer>
                    </FriendsUserContainer>
                    <PostsUserContainer>
                        <h2> {`Posts (${ user.posts.length })`}</h2>
                        <PostsComponent records={ user.posts } writable={ false }/>
                    </PostsUserContainer>
                </UserContentContainer> }
            </UserViewContainer>
        </LayoutUserView>
    );
};

export default UserView;
