import styled from "styled-components";
import {DEVICE, LAYOUT} from "../../../common/Constants";

export const PostsWrapper = styled.div`
  display: flex;
  justify-content: center;
  grid-column: 2/3;
  background-color: #a2c8ec;
`;

export const FriendsContainer = styled.div`
  grid-column: 3/4;
  grid-row: 2/3;
  max-height: 100vh;
`;

export const MostPopularPostsContainer = styled.div`
  grid-column: 1/2;
  max-height: calc(100vh - 5rem);
`;

export const HomeViewContainer = styled.div`
  display: grid;
  grid-template-columns: 30rem 1fr 30rem;
  grid-template-rows: ${LAYOUT.navigationHeight} 1fr;
  width: 100%;
  min-height: 200vh;
  max-height: max-content;

  @media (max-width: ${DEVICE.large}) {
    grid-template-columns: 1fr;
    
    ${PostsWrapper} {
      grid-column-start: 1;
    } 
    
    ${MostPopularPostsContainer} {
      display: none;
    }

    .section-friends {
      display: none;
    }

    .section-top-posts {
      display: none;
    }
    
    ${FriendsContainer} {
      display: none;
    }
  }
`

export const PostsContainer = styled.div`
  width: min(100%, 65rem);
  padding: 0 1.5rem; 
`;

export const NavigationContainer = styled.div`
  position: sticky;
  top: 0;
  grid-column: 1/4;
  z-index: 100;
  background-color: #5c83a5 ;
`;
