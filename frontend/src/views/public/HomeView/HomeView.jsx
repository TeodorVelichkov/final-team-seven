import React, {useEffect, useState} from "react";
import {
  // FriendsContainer,
  HomeViewContainer,
  // MostPopularPostsContainer,
  NavigationContainer,
  PostsContainer, PostsWrapper
} from "./HomeViewStyles";
import "../../../components/public/forms/comment-form/CommentForm.css";
import Navigation from "../../../components/common/navigation/NavigationComponent";
import UserFriendsSection from "../../../components/public/user-friends-section/UserFriendsSection";
import PostsComponent from "../../../components/public/posts/PostsComponent";
import {getAllFeed} from "../../../services/fetch-service";
import {getAuthToken} from "../../../services/AuthService";
import TopPostsSection from "../../../components/public/top-posts-section/TopPostsSection";

const HomeView = () => {

  const [ posts, setPosts ] = useState([]);
  const [ isLoading, setIsLoading ] = useState(true);

  useEffect(() => {
    getAllFeed(undefined, getAuthToken())
        .then(response => {
            setPosts(response);
            setIsLoading(false)
        });
  }, []);

  return (
        <HomeViewContainer>
            <NavigationContainer>
                <Navigation/>
            </NavigationContainer>
          <TopPostsSection/>
        <PostsWrapper>
          <PostsContainer>
            { (!isLoading) && <PostsComponent records={posts}/> }
          </PostsContainer>
        </PostsWrapper>
        <UserFriendsSection/>
      </HomeViewContainer>
  );
};

export default HomeView;
