import React from 'react';
import {Switch, useRouteMatch} from "react-router-dom";
import {AdminViewContent, AdminViewWrapper} from "./AdminViewStyles";
import PrivateRoute from "../../../common/authentication/PrivateRoute";
import {USER_ROLES} from "../../../common/Constants";
import NavigationComponent from "../../../components/admin/navigation/NavigationComponent";
import HomeView from "../HomeView/HomeView";
import UserManagementComponent from "../../../components/admin/users-management/UserManagementComponent";
import PostsManage from "../../../components/admin/posts-management/PostsManagementComponent";

const AdminView = () => {
    const { path } = useRouteMatch();

    return (
        <AdminViewWrapper>
            <NavigationComponent/>
            <AdminViewContent>
                <Switch>
                    <PrivateRoute exact path={`${path}/users/:id/posts`} roles={[ USER_ROLES.ADMIN ]} redirect={`/`} component={ PostsManage }/>
                    <PrivateRoute exact path={`${path}/users/:id`} roles={[ USER_ROLES.ADMIN ]} redirect={`/`} component={ UserManagementComponent }/>
                    <PrivateRoute exact path={`${path}/users`} roles={[ USER_ROLES.ADMIN ]} redirect={`/`} component={ UserManagementComponent }/>
                    <PrivateRoute exact path={`${path}`} roles={[ USER_ROLES.ADMIN ]} redirect={`/`} component={ HomeView }/>
                </Switch>
            </AdminViewContent>
        </AdminViewWrapper>
    );
};

export default AdminView;
