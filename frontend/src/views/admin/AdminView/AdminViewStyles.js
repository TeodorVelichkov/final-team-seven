import styled from "styled-components";

export const AdminViewWrapper = styled.div`
  position: relative;
  display: grid;
  grid-template-columns: 15rem 1fr;
  min-height: 100vh;
`;

export const AdminViewContent = styled.div`
  display: grid;
  place-items: center;
`;
